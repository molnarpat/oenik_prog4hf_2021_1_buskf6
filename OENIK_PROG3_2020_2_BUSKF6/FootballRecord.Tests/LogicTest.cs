﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;
    using FootballRecord.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic Test class.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IPlayerRepository> playerMockRepo;
        private Mock<ICoachRepository> coachMockRepo;
        private Mock<ITeamRepository> teamMockRepo;
        private List<AveragesResult> expectedAverages;
        private List<Team> expextedYoungestTeam;
        private List<Player> expectedLowestbankrollplayers;

        private PlayerLogic playerLogic;
        private TeamLogic teamLogic;
        private CoachLogic coachLogic;

        /// <summary>
        /// Setup the Logics, Mockrepos.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            this.playerMockRepo = new Mock<IPlayerRepository>();
            this.teamMockRepo = new Mock<ITeamRepository>();
            this.coachMockRepo = new Mock<ICoachRepository>();
            var players = new List<Player>()
            {
                new Player() { Surname = "Patrik", Firstname = "Molnar", JerseyNumber = 99, DateOfBirth = "1997.05.20", Performance = 99, TeamId = 1 },
                new Player() { Surname = "József", Firstname = "Molnar", JerseyNumber = 00, DateOfBirth = "1996.05.21", Performance = 10, TeamId = 2 },
            };

            this.playerMockRepo.Setup(x => x.GetAll()).Returns(players.AsQueryable);
            var teams = new List<Team>()
                {
               new Team() { TeamId = 10, Name = "Patya FC", Founded = "2020", Stadium = "Kispesti Arena", BankRoll = 1000000 },
               new Team() { TeamId = 20, Name = "Patyomkim CF", Founded = "2002", Stadium = "Rákosmenti Sportcsarnok", BankRoll = 99999 },
                };

            this.teamMockRepo.Setup(x => x.GetAll()).Returns(teams.AsQueryable);
            var coaches = new List<Coach>()
                {
               new Coach() { CoachId = 1, Name = "PowerCoachJuan", Salary = 9999, Nationality = "Hungarian", AddPerformance = 7, Age = 99, PlayerId = 12 },
               new Coach() { CoachId = 2, Name = "GenezisCoachPablo", Salary = 3333, Nationality = "Japanese", AddPerformance = 9, Age = 11, PlayerId = 24 },
                };

            this.coachMockRepo.Setup(x => x.GetAll()).Returns(coaches.AsQueryable);

            this.playerLogic = new PlayerLogic(this.playerMockRepo.Object, this.coachMockRepo.Object, this.teamMockRepo.Object);
            this.teamLogic = new TeamLogic(this.playerMockRepo.Object, this.teamMockRepo.Object, this.coachMockRepo.Object);
            this.coachLogic = new CoachLogic(this.coachMockRepo.Object);
        }

        /// <summary>
        /// Test Player Add.
        /// </summary>
        [Test]
        public void AddPlayerTest()
        {
            this.playerLogic.AddPlayer("Patyesz", "Szazados", 99, "1997.05.05.", 90, 1);
            this.playerMockRepo.Verify(x => x.CreatePlayer(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test Delete Player.
        /// </summary>
        [Test]
        public void DeletePlayerTest()
        {
            this.playerLogic.DeletePlayer(1);
            this.playerLogic.DeletePlayer(2);

            this.playerMockRepo.Verify(x => x.DeletePlayer(It.IsAny<int>()), Times.Exactly(2));
        }

        /// <summary>
        /// Test GetAllTeams.
        /// </summary>
        [Test]
        public void GetAllTeamsTest()
        {
            this.teamLogic.GetAllTeams();

            this.teamMockRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// A test case that tests the change in the coach’s salary.
        /// </summary>
        [Test]
        public void ChangeCoachSalaryTest()
        {
            this.coachLogic.ChangeCoachSalary(1, 3333);
            this.coachMockRepo.Verify(x => x.UpdateSalary(1, It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// A test case that tests a player's query.
        /// </summary>
        [Test]
        public void GetOnePlayerTest()
        {
            this.playerLogic.GetPlayerById(1);
            this.playerMockRepo.Verify(x => x.GetById(1), Times.Once);
        }

        /// <summary>
        /// A test case that tests a change in a player’s performance.
        /// </summary>
        [Test]
        public void UpdatePlayerPerformanceTest()
        {
            this.playerLogic.ChangePlayerPerformance(1, 89);
            this.playerMockRepo.Verify(x => x.UpdatePerformance(1, It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// A test case that tests the deletion of a team.
        /// </summary>
        [Test]
        public void DeleteTeamTest()
        {
            this.teamLogic.DeleteTeam(1);
            this.teamMockRepo.Verify(x => x.DeleteTeam(It.IsAny<int>()), Times.Exactly(1));
        }

        /// <summary>
        /// A test case that tests the team’s average performance.
        /// </summary>
        [Test]
        public void TeamAveragePerformanceTest()
        {
            var logic = this.CreateLogicWithMocks();
            var actualAverages = logic.GetAveragePerformance();

            Assert.That(actualAverages, Is.EquivalentTo(this.expectedAverages));
            this.playerMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
            this.teamMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
        }

        /// <summary>
        /// A test case that tests the query of the youngest teams.
        /// </summary>
        [Test]
        public void YoungestTeamTest()
        {
            var logic = this.CreateLogicWithMocks2();
            var actualYoungestTeams = logic.GetYoungestTeam();

            Assert.IsTrue(this.expextedYoungestTeam.SequenceEqual(actualYoungestTeams));
            this.playerMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
            this.teamMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
        }

        /// <summary>
        /// A test case that tests the players on the lowest budget team.
        /// </summary>
        [Test]
        public void LowestBankrollPlayers()
        {
            var logic = this.CreateLogicWithMocks3();
            var actualLowestBankrollPlayers = logic.GetPlayersLowestBankRollTeam();

            Assert.That(actualLowestBankrollPlayers, Is.EquivalentTo(this.expectedLowestbankrollplayers));
            this.playerMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
            this.teamMockRepo.Verify(x => x.GetAll(), Times.Exactly(1));
            this.coachMockRepo.Verify(x => x.GetAll(), Times.Never);
        }

        /// <summary>
        /// Helper for non-crud test cases.
        /// </summary>
        /// <returns>TeamLogic.</returns>
        private TeamLogic CreateLogicWithMocks()
        {
            this.playerMockRepo = new Mock<IPlayerRepository>();
            this.teamMockRepo = new Mock<ITeamRepository>();
            this.coachMockRepo = new Mock<ICoachRepository>();

            Team fradi = new Team() { TeamId = 1, Name = "Ferencvaros", Founded = "1111", Stadium = "Groupama", BankRoll = 111 };
            Team ujpest = new Team() { TeamId = 2, Name = "Ujpest", Founded = "1222", Stadium = "Szusza", BankRoll = 222 };
            List<Team> teams = new List<Team>() { fradi, ujpest };
            List<Player> players = new List<Player>()
            {
            new Player() { PlayerId = 1, Surname = "Molnar", Firstname = "Patrik", JerseyNumber = 99, DateOfBirth = "1997.05.20.", Performance = 80, TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 2, Surname = "Gergely", Firstname = "Zoltan", JerseyNumber = 00, DateOfBirth = "2004.04.13.", Performance = 60, TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 3, Surname = "Janos", Firstname = "Bela", JerseyNumber = 11, DateOfBirth = "2000.01.03.", Performance = 50, TeamId = ujpest.TeamId, Team = ujpest },
            };

            this.expectedAverages = new List<AveragesResult>()
            {
                new AveragesResult() { TeamName = "Ferencvaros", AveragePerformance = 70 },
                new AveragesResult() { TeamName = "Ujpest", AveragePerformance = 50 },
            };

            this.playerMockRepo.Setup(repo => repo.GetAll()).Returns(players.AsQueryable());
            this.teamMockRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            return new TeamLogic(this.playerMockRepo.Object, this.teamMockRepo.Object, this.coachMockRepo.Object);
        }

        /// <summary>
        /// Helper for non-crud test cases.
        /// </summary>
        /// <returns>TeamLogic.</returns>
        private TeamLogic CreateLogicWithMocks2()
        {
            this.playerMockRepo = new Mock<IPlayerRepository>();
            this.teamMockRepo = new Mock<ITeamRepository>();
            this.coachMockRepo = new Mock<ICoachRepository>();

            Team fradi = new Team() { TeamId = 1, Name = "Ferencvaros" };
            Team ujpest = new Team() { TeamId = 2, Name = "Ujpest" };
            List<Team> teams = new List<Team>() { fradi, ujpest };

            List<Player> players = new List<Player>()
            {
            new Player() { PlayerId = 1, Surname = "Molnar", Firstname = "Patrik", DateOfBirth = "1997.05.20.", TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 2, Surname = "Gergely", Firstname = "Zoltan", DateOfBirth = "2004.05.20.", TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 3, Surname = "Janos", Firstname = "Bela", DateOfBirth = "2000.05.20.", TeamId = ujpest.TeamId, Team = ujpest },
            };

            this.expextedYoungestTeam = new List<Team>()
            {
                new Team() { TeamId = 1, Name = "Ferencvaros" },
                new Team() { TeamId = 2, Name = "Ujpest" },
            };

            this.playerMockRepo.Setup(repo => repo.GetAll()).Returns(players.AsQueryable());
            this.teamMockRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            return new TeamLogic(this.playerMockRepo.Object, this.teamMockRepo.Object, this.coachMockRepo.Object);
        }

        /// <summary>
        /// Helper for non-crud test cases.
        /// </summary>
        /// <returns>TeamLogic.</returns>
        private PlayerLogic CreateLogicWithMocks3()
        {
            this.playerMockRepo = new Mock<IPlayerRepository>();
            this.teamMockRepo = new Mock<ITeamRepository>();
            this.coachMockRepo = new Mock<ICoachRepository>();

            Team fradi = new Team() { TeamId = 1, Name = "Ferencvaros", Founded = "1111", Stadium = "Groupama", BankRoll = 111 };
            Team ujpest = new Team() { TeamId = 2, Name = "Ujpest", Founded = "1222", Stadium = "Szusza", BankRoll = 222 };
            List<Team> teams = new List<Team>() { fradi, ujpest };
            List<Player> players = new List<Player>()
            {
            new Player() { PlayerId = 1, Surname = "Molnar", Firstname = "Patrik", JerseyNumber = 99, DateOfBirth = "1997.05.20.", Performance = 80, TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 2, Surname = "Gergely", Firstname = "Zoltan", JerseyNumber = 00, DateOfBirth = "2004.04.13.", Performance = 60, TeamId = fradi.TeamId, Team = fradi },
            new Player() { PlayerId = 3, Surname = "Janos", Firstname = "Bela", JerseyNumber = 11, DateOfBirth = "2000.01.03.", Performance = 50, TeamId = ujpest.TeamId, Team = ujpest },
            };

            this.expectedLowestbankrollplayers = new List<Player>()
            {
                new Player() { PlayerId = 1, Surname = "Molnar", Firstname = "Patrik", JerseyNumber = 99, DateOfBirth = "1997.05.20.", Performance = 80, TeamId = fradi.TeamId, Team = fradi },
                new Player() { PlayerId = 2, Surname = "Gergely", Firstname = "Zoltan", JerseyNumber = 00, DateOfBirth = "2004.04.13.", Performance = 60, TeamId = fradi.TeamId, Team = fradi },
            };
            this.playerMockRepo.Setup(repo => repo.GetAll()).Returns(players.AsQueryable());
            this.teamMockRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            return new PlayerLogic(this.playerMockRepo.Object, this.coachMockRepo.Object, this.teamMockRepo.Object);
        }
    }
}
