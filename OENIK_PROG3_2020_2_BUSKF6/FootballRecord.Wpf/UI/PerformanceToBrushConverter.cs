﻿namespace FootballRecord.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Converted the performance to the background color.
    /// </summary>
    public class PerformanceToBrushConverter : IValueConverter
    {
        /// <summary>
        /// The method that changed the background color.
        /// </summary>
        /// <param name="value">The object we want to cast.</param>
        /// <param name="targetType">TargetType.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="culture">Culture.</param>
        /// <returns>Returns the background color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int performance = (int)value;
            if (performance > 80)
            {
                return Brushes.LightGreen;
            }
            else
            {
                return Brushes.Red;
            }
        }

        /// <summary>
        /// ConvertBack method.
        /// </summary>
        /// <param name="value">The object we want to cast.</param>
        /// <param name="targetType">TargetType.</param>
        /// <param name="parameter">Parameter.</param>
        /// <param name="culture">Culture.</param>
        /// <returns>Returns DoNothing method.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
