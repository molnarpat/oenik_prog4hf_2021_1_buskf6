﻿namespace FootballRecord.Wpf.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Wpf.BL;
    using FootballRecord.Wpf.DATA;

    /// <summary>
    /// Edit via Window.
    /// </summary>
    public class EditorServiceViaWindows : IPlayerEditor
    {
        /// <summary>
        /// Edit the player via Window.
        /// </summary>
        /// <param name="p">Player.</param>
        /// <returns>Returns a boolean that the modify was succesful.</returns>
        public bool EditPlayer(PlayerUI p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}
