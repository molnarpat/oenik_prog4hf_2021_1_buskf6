﻿namespace FootballRecord.Wpf.DATA
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// PlayerUI class.
    /// </summary>
    public class PlayerUI : ObservableObject
    {
        private string surname;
        private string firstname;
        private int jerseyNumber;
        private string dateOfBirth;
        private int performance;

        /// <summary>
        /// Gets or sets PlayerId - Id tag.
        /// </summary>
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets surname of player.
        /// </summary>
        public string Surname
        {
            get { return surname; }
            set { Set(ref surname, value); }
        }

        /// <summary>
        /// Gets or sets firstname of player.
        /// </summary>
        public string Firstname
        {
            get { return firstname; }
            set { Set(ref firstname, value); }
        }

        /// <summary>
        /// Gets or sets jerseyNumber of player.
        /// </summary>
        public int JerseyNumber
        {
            get { return jerseyNumber; }
            set { Set(ref jerseyNumber, value); }
        }

        /// <summary>
        /// Gets or sets dateOfBirth of player.
        /// </summary>
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { Set(ref dateOfBirth, value); }
        }

        /// <summary>
        /// Gets or sets performance of player.
        /// </summary>
        public int Performance
        {
            get { return performance; }
            set { Set(ref performance, value); }
        }

        /// <summary>
        /// Gets or sets Foreign key teamId.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerUI"/> class.
        /// </summary>
        public PlayerUI()
        {
        }

        /// <summary>
        /// Cloning method.
        /// </summary>
        /// <param name="other">Other entity.</param>
        public void CopyFrom(PlayerUI other)
        {
            this.Surname = other.Surname;
            this.Firstname = other.Firstname;
            this.JerseyNumber = other.JerseyNumber;
            this.DateOfBirth = other.DateOfBirth;
            this.Performance = other.Performance;
        }

        /// <summary>
        /// Cloning method with id.
        /// </summary>
        /// <param name="other">Other entity.</param>
        public void CopyFromId(PlayerUI other)
        {
            this.PlayerId = other.PlayerId;
            CopyFrom(other);
        }
    }
}
