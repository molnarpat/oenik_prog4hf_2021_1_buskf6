﻿namespace FootballRecord.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Wpf.DATA;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// EditorViewModel for EditorWindow.
    /// </summary>
    public partial class EditorViewModel : ViewModelBase
    {
        private PlayerUI player;

        /// <summary>
        /// Gets or sets player.
        /// </summary>
        public PlayerUI Player
        {
            get { return player; }
            set { Set(ref player, value); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            player = new PlayerUI();
            if (IsInDesignMode)
            {
                player.Firstname = "Molnár";
                player.Surname = "Patrik";
                player.JerseyNumber = 12;
                player.Performance = 99;
                player.DateOfBirth = "1997.05.20";
            }
        }
    }
}
