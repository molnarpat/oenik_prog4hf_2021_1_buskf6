﻿namespace FootballRecord.Wpf.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using FootballRecord.Logic;
    using FootballRecord.Wpf.BL;
    using FootballRecord.Wpf.DATA;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainViewModel for MainWindow.
    /// </summary>
    public partial class MainViewModel : ViewModelBase
    {
        private IPlayerWPFLogic logic;

        /// <summary>
        /// Gets players are stored in this Collection.
        /// </summary>
        public ObservableCollection<PlayerUI> Players { get; private set; }

        private PlayerUI selectedPlayer;

        /// <summary>
        /// Gets or sets SelectedPlayer.
        /// </summary>
        public PlayerUI SelectedPlayer
        {
            get { return selectedPlayer; }
            set { Set(ref selectedPlayer, value); }
        }

        /// <summary>
        /// Gets addcmd.
        /// </summary>
        public ICommand Addcmd { get; private set; }

        /// <summary>
        /// Gets modcmd.
        /// </summary>
        public ICommand Modcmd { get; private set; }

        /// <summary>
        /// Gets delcmd.
        /// </summary>
        public ICommand Delcmd { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">PlayerWPF Logic.</param>
        public MainViewModel(IPlayerWPFLogic logic)
        {
            this.logic = logic;
            Players = new ObservableCollection<PlayerUI>();
            if (IsInDesignMode)
            {
                PlayerUI p2 = new PlayerUI() { Surname = "Kiss", Firstname = "Róbert", Performance = 10 };
                PlayerUI p3 = new PlayerUI() { Surname = "Puskás", Firstname = "Ferenc", DateOfBirth = "1959.05.20", JerseyNumber = 9, Performance = 99 };
                Players.Add(p2);
                Players.Add(p3);
            }
            else
            {
                this.logic.GetAllPlayers().ToList().ForEach(x => Players.Add(x));
            }

            Addcmd = new RelayCommand(() => this.logic.AddPlayer(this.Players));
            Modcmd = new RelayCommand(() => this.logic.ModPlayer(this.SelectedPlayer));
            Delcmd = new RelayCommand(() => this.logic.DelPlayer(this.Players, this.SelectedPlayer));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel() : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IPlayerWPFLogic>())
        {
        }
    }
}
