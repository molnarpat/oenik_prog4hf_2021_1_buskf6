﻿namespace FootballRecord.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Data;
    using FootballRecord.Data.Models;
    using FootballRecord.Logic;
    using FootballRecord.Wpf.DATA;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// PlayerWPF logic class.
    /// </summary>
    public class PlayerWPFLogic : IPlayerWPFLogic
    {
        private IPlayerEditor editorService;
        private IMessenger messengerService;
        private IPlayerLogic playerlogic;
        private ITeamLogic teamlogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerWPFLogic"/> class.
        /// Created PlayerWPFLogic with the interfaces.
        /// </summary>
        /// <param name="editorService">Editorservice.</param>
        /// <param name="messengerService">MessengerService.</param>
        /// <param name="playerLogic">Player Logic.</param>
        /// <param name="teamLogic">Team Logic.</param>
        public PlayerWPFLogic(IPlayerEditor editorService, IMessenger messengerService, IPlayerLogic playerLogic, ITeamLogic teamLogic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.playerlogic = playerLogic;
            this.teamlogic = teamLogic;
        }

        /// <summary>
        /// Add a player.
        /// </summary>
        /// <param name="players">The List we want to add in.</param>
        public void AddPlayer(IList<PlayerUI> players)
        {
            PlayerUI newPlayer = new PlayerUI();
            if (editorService.EditPlayer(newPlayer) == true)
            {
                this.playerlogic.AddPlayer(newPlayer.Surname, newPlayer.Firstname, newPlayer.JerseyNumber, newPlayer.DateOfBirth, newPlayer.Performance, 1);
                newPlayer.PlayerId = this.playerlogic.GetAllPlayers().Last().PlayerId;
                players?.Add(newPlayer);
                messengerService.Send("PLAYER ADDED", "LogicResult");
            }
            else
            {
                messengerService.Send("ADD IS CANCELED!", "LogicResult");
            }
        }

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="players">The list we want to delete from.</param>
        /// <param name="player">The player to be deleted.</param>
        public void DelPlayer(IList<PlayerUI> players, PlayerUI player)
        {
            if (player != null && players.Remove(player))
            {
                this.playerlogic.DeletePlayer(player.PlayerId);
                messengerService.Send("PLAYER DELETED!", "LogicResult");
            }
            else
            {
                messengerService.Send("DELETE CANCELED!", "LogicResult");
            }
        }

        /// <summary>
        /// GetAllPlayers method.
        /// </summary>
        /// <returns>Return all players from the database.</returns>
        public IList<PlayerUI> GetAllPlayers()
        {
            List<Player> players = this.playerlogic.GetAllPlayers().ToList();
            List<PlayerUI> list = new List<PlayerUI>();

            foreach (var player in players)
            {
                list.Add(new PlayerUI()
                {
                    PlayerId = player.PlayerId,
                    Firstname = player.Firstname,
                    Surname = player.Surname,
                    Performance = player.Performance,
                    DateOfBirth = player.DateOfBirth,
                    JerseyNumber = player.JerseyNumber,
                    TeamId = player.TeamId,
                });
            }

            return list;
        }

        /// <summary>
        /// Modify player.
        /// </summary>
        /// <param name="playerToModify">The player that we want to modify.</param>
        public void ModPlayer(PlayerUI playerToModify)
        {
            if (playerToModify == null)
            {
                messengerService.Send("PLAYER MODIFY FAILED!", "LogicResult");
                return;
            }

            PlayerUI clone = new PlayerUI();
            clone.CopyFromId(playerToModify);
            if (editorService.EditPlayer(clone) == true)
            {
                playerToModify.CopyFromId(clone);

                playerlogic.UpdatePlayer(
                    playerToModify.PlayerId,
                    playerToModify.Surname,
                    playerToModify.Firstname,
                    playerToModify.JerseyNumber,
                    playerToModify.DateOfBirth,
                    playerToModify.Performance);

                messengerService.Send("PLAYER MODIFIED!", "LogicResult");
            }
            else
            {
                messengerService.Send("PLAYER MODIFY CANCELED!", "LogicResult");
            }
        }
    }
}
