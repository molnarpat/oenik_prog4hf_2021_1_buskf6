﻿namespace FootballRecord.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Wpf.DATA;

    /// <summary>
    /// Interface for the ServiceEditor.
    /// </summary>
    public interface IPlayerEditor
    {
        /// <summary>
        /// Modify a Player.
        /// </summary>
        /// <param name="p">A player to be modified.</param>
        /// <returns>Boolean that tells the edit was successful.</returns>
        bool EditPlayer(PlayerUI p);
    }
}
