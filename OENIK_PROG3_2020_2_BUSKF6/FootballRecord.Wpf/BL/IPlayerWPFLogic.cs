﻿namespace FootballRecord.Wpf.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Wpf.DATA;

    /// <summary>
    /// WPFLogic Interface.
    /// </summary>
    public interface IPlayerWPFLogic
    {
        /// <summary>
        /// Add Player method.
        /// </summary>
        /// <param name="players">Player list to which we want to add.</param>
        void AddPlayer(IList<PlayerUI> players);

        /// <summary>
        /// Player modify method.
        /// </summary>
        /// <param name="playerToModify">Player we want to modify.</param>
        void ModPlayer(PlayerUI playerToModify);

        /// <summary>
        /// Player delete method.
        /// </summary>
        /// <param name="players">From this collection we want to delete the Player.</param>
        /// <param name="player">Player we want to delete.</param>
        void DelPlayer(IList<PlayerUI> players, PlayerUI player);

        /// <summary>
        /// GetAll method.
        /// </summary>
        /// <returns>A List with the players.</returns>
        IList<PlayerUI> GetAllPlayers();
    }
}
