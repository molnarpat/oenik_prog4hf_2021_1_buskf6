﻿namespace FootballRecord.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using FootballRecord.Data.Models;
    using FootballRecord.Logic;
    using FootballRecord.Repository;
    using FootballRecord.Wpf.BL;
    using FootballRecord.Wpf.UI;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        private class MyIOC : SimpleIoc, IServiceLocator
        {
            public static MyIOC Instance { get; private set; } = new MyIOC();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIOC.Instance);
            MyIOC.Instance.Register<IPlayerLogic, PlayerLogic>();
            MyIOC.Instance.Register<IPlayerRepository, PlayerRepository>();
            MyIOC.Instance.Register<FootballDBContext, FootballDBContext>();
            MyIOC.Instance.Register<ICoachRepository, CoachRepository>();
            MyIOC.Instance.Register<ITeamRepository, TeamRepository>();
            MyIOC.Instance.Register<ITeamLogic, TeamLogic>();
            MyIOC.Instance.Register<ICoachLogic, CoachLogic>();

            MyIOC.Instance.Register<IPlayerEditor, EditorServiceViaWindows>();
            MyIOC.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIOC.Instance.Register<IPlayerWPFLogic, PlayerWPFLogic>();
        }
    }
}
