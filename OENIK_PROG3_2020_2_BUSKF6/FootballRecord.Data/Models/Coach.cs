﻿// <copyright file="Coach.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Coach class for coach objects.
    /// </summary>
    [Table("Coaches")]
    public class Coach
    {
        /// <summary>
        /// Gets or sets coach table Key.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CoachId { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Salary.
        /// </summary>
        public int Salary { get; set; }

        /// <summary>
        /// Gets or sets nationality.
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets addPerformance.
        /// </summary>
        public int AddPerformance { get; set; }

        /// <summary>
        /// Gets or sets age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets player.
        /// </summary>
        [NotMapped]
        public virtual Player Player { get; set; }

        /// <summary>
        /// Gets or sets playerId foreign key.
        /// </summary>
        [ForeignKey(nameof(Player))]
        public int PlayerId { get; set; }

        /// <summary>
        /// ToString method.
        /// </summary>
        /// <returns>The coach data to string.</returns>
        public override string ToString()
        {
            return $" ID:[{this.CoachId}] -> Nev: {this.Name} | Fizetes: {this.Salary} | Nemzetiseg: {this.Nationality} | Teljesitmenynoveles: {this.AddPerformance} | Kor: {this.Age} | JatekosID: {this.PlayerId}";
        }

        /// <summary>
        /// required for tests.
        /// </summary>
        /// <param name="obj">Obj.</param>
        /// <returns>Returns true if the 2 object is equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Coach)
            {
                Coach other = obj as Coach;
                return this.CoachId == other.CoachId &&
                    this.Name == other.Name &&
                    this.Salary == other.Salary &&
                    this.Nationality == other.Nationality &&
                    this.AddPerformance == other.AddPerformance &&
                    this.Age == other.Age &&
                    this.PlayerId == other.PlayerId
                    ;
            }

            return false;
        }

        /// <summary>
        /// Required for tests.
        /// </summary>
        /// <returns>Returns HashCode.</returns>
        public override int GetHashCode()
        {
            return this.CoachId;
        }
    }
}
