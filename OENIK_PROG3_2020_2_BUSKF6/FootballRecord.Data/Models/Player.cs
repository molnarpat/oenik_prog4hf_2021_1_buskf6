﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Player object class.
    /// </summary>
    [Table("Players")]
    public class Player
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {
            this.Coaches = new HashSet<Coach>();
        }

        /// <summary>
        /// Gets or sets PlayerId - Id tag.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets surname of player.
        /// </summary>
        [MaxLength(50)]
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets firstname of player.
        /// </summary>
        [MaxLength(50)]
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets jerseyNumber of player.
        /// </summary>
        public int JerseyNumber { get; set; }

        /// <summary>
        /// Gets or sets dateOfBirth of player.
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets performance of player.
        /// </summary>
        public int Performance { get; set; }

        /// <summary>
        /// Gets or sets team.
        /// </summary>
        [NotMapped]
        public virtual Team Team { get; set; }

        /// <summary>
        /// Gets or sets Foreign key teamId.
        /// </summary>
        [ForeignKey(nameof(Team))]
        public int TeamId { get; set; }

        /// <summary>
        /// Gets coaches.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Coach> Coaches { get; }

        /// <summary>
        /// ToString method.
        /// </summary>
        /// <returns>The player data to string.</returns>
        public override string ToString()
        {
            return $"ID: [{this.PlayerId}] -> Nev: {this.Surname} {this.Firstname} | Mezszam: {this.JerseyNumber} | Szuletesi datum: {this.DateOfBirth} | Teljesitmeny: {this.Performance} | TEAMID: {this.TeamId}";
        }

        /// <summary>
        /// Compares 2 objects for tests.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns>Boolean that the two object equals.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Player)
            {
                Player other = obj as Player;
                return this.PlayerId == other.PlayerId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>Return 0.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
