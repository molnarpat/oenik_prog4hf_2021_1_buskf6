﻿// <copyright file="FootballDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// FootballDBContext makes a frame for the 3 tables.
    /// </summary>
    public class FootballDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FootballDBContext"/> class.
        /// </summary>
        public FootballDBContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets has the Teams table.
        /// </summary>
        public virtual DbSet<Team> Teams { get; set; }

        /// <summary>
        /// Gets or sets has the Players table.
        /// </summary>
        public virtual DbSet<Player> Players { get; set; }

        /// <summary>
        /// Gets or sets has the Coaches table.
        /// </summary>
        public virtual DbSet<Coach> Coaches { get; set; }

        /// <summary>
        /// Overriding Onconfiguring method.
        /// </summary>
        /// <param name="optionsBuilder"> optionsbuilder. </param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\FootballDB.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// OnModelCreating overrided.
        /// </summary>
        /// <param name="modelBuilder"> modelBuilder. </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Coach coach1 = new Coach() { CoachId = 1, Name = "StaminaCoach", Salary = 3000, Nationality = "Hungary", AddPerformance = 5, Age = 45, PlayerId = 5 };
            Coach coach2 = new Coach() { CoachId = 2, Name = "AgilityCoach", Salary = 4500, Nationality = "Spanish", AddPerformance = 7, Age = 33, PlayerId = 10 };
            Coach coach3 = new Coach() { CoachId = 3, Name = "TacticalCoach", Salary = 6000, Nationality = "French", AddPerformance = 9, Age = 29, PlayerId = 15 };
            Coach coach4 = new Coach() { CoachId = 4, Name = "MentalCoach", Salary = 2000, Nationality = "Italian", AddPerformance = 4, Age = 41, PlayerId = 20 };
            Coach coach5 = new Coach() { CoachId = 5, Name = "GoalkeeperCoach", Salary = 3500, Nationality = "Belgian", AddPerformance = 1, Age = 56, PlayerId = 25 };
            Coach coach6 = new Coach() { CoachId = 6, Name = "StrikerCoach", Salary = 4000, Nationality = "Polish", AddPerformance = 6, Age = 31, PlayerId = 30 };
            Coach coach7 = new Coach() { CoachId = 7, Name = "DefenderCoach", Salary = 2500, Nationality = "English", AddPerformance = 2, Age = 40, PlayerId = 35 };
            Coach coach8 = new Coach() { CoachId = 8, Name = "MidfielderCoach", Salary = 5500, Nationality = "German", AddPerformance = 3, Age = 39, PlayerId = 40 };

            Team t1 = new Team() { TeamId = 1, Name = "FC Barcelona", Founded = "1899", Stadium = "Camp Nou", BankRoll = 120000 };
            Team t2 = new Team() { TeamId = 2, Name = "Real Madrid CF", Founded = "1902", Stadium = "Santiago Bernabéu", BankRoll = 91000 };
            Team t3 = new Team() { TeamId = 3, Name = "Atlético de Madrid", Founded = "1903", Stadium = "Wanda Metropolitano", BankRoll = 78000 };
            Team t4 = new Team() { TeamId = 4, Name = "Villareal CF", Founded = "1923", Stadium = "Estadio El Madrigal", BankRoll = 61000 };
            Team t5 = new Team() { TeamId = 5, Name = "Granada CF", Founded = "1931", Stadium = "Nuevo Los Cármenes", BankRoll = 59000 };
            Team t6 = new Team() { TeamId = 6, Name = "Real Sociedad", Founded = "1909", Stadium = "Estadio Anoeta", BankRoll = 64000 };
            Team t7 = new Team() { TeamId = 7, Name = "Sevilla FC", Founded = "1905", Stadium = "Estadio Ramón Sánchez", BankRoll = 66000 };
            Team t8 = new Team() { TeamId = 8, Name = "Athletic Bilbao", Founded = "1898", Stadium = "San Mamés Stadion", BankRoll = 51000 };

            Player player1 = new Player() { PlayerId = 1, Surname = "ter Stegen", Firstname = "Marc-André", JerseyNumber = 1, DateOfBirth = "1992.04.30", Performance = 90, TeamId = t1.TeamId };
            Player player2 = new Player() { PlayerId = 2, Surname = "Piqué", Firstname = "Gerard", JerseyNumber = 3, DateOfBirth = "1987.02.02", Performance = 85, TeamId = t1.TeamId };
            Player player3 = new Player() { PlayerId = 3, Surname = "de Jong", Firstname = "Frenkie", JerseyNumber = 21, DateOfBirth = "1997.05.12", Performance = 87, TeamId = t1.TeamId };
            Player player4 = new Player() { PlayerId = 4, Surname = "Coutinho", Firstname = "Philippe", JerseyNumber = 14, DateOfBirth = "1992.06.12", Performance = 89, TeamId = t1.TeamId };
            Player player5 = new Player() { PlayerId = 5, Surname = "Messi", Firstname = "Lionel", JerseyNumber = 10, DateOfBirth = "1987.06.24", Performance = 96, TeamId = t1.TeamId };
            Player player6 = new Player() { PlayerId = 6, Surname = "Courtois", Firstname = "Thibaut", JerseyNumber = 1, DateOfBirth = "1992.05.11", Performance = 91, TeamId = t2.TeamId };
            Player player7 = new Player() { PlayerId = 7, Surname = "Ramos", Firstname = "Sergio", JerseyNumber = 4, DateOfBirth = "1986.03.30", Performance = 88, TeamId = t2.TeamId };
            Player player8 = new Player() { PlayerId = 8, Surname = "Kroos", Firstname = "Toni", JerseyNumber = 8, DateOfBirth = "1990.01.04", Performance = 81, TeamId = t2.TeamId };
            Player player9 = new Player() { PlayerId = 9, Surname = "Modric", Firstname = "Luka", JerseyNumber = 10, DateOfBirth = "1985.09.09", Performance = 79, TeamId = t2.TeamId };
            Player player10 = new Player() { PlayerId = 10, Surname = "Hazard", Firstname = "Eden", JerseyNumber = 7, DateOfBirth = "1991.01.07", Performance = 75, TeamId = t2.TeamId };
            Player player11 = new Player() { PlayerId = 11, Surname = "Oblak", Firstname = "Jan", JerseyNumber = 13, DateOfBirth = "1993.01.07", Performance = 88, TeamId = t3.TeamId };
            Player player12 = new Player() { PlayerId = 12, Surname = "Giménez", Firstname = "José", JerseyNumber = 2, DateOfBirth = "1995.01.20", Performance = 83, TeamId = t3.TeamId };
            Player player13 = new Player() { PlayerId = 13, Surname = "Morata", Firstname = "Álvaro", JerseyNumber = 7, DateOfBirth = "1992.10.23", Performance = 81, TeamId = t3.TeamId };
            Player player14 = new Player() { PlayerId = 14, Surname = "Koke", Firstname = "Merodio", JerseyNumber = 6, DateOfBirth = "1992.01.08", Performance = 77, TeamId = t3.TeamId };
            Player player15 = new Player() { PlayerId = 15, Surname = "Félix", Firstname = "Joao", JerseyNumber = 7, DateOfBirth = "1999.10.10", Performance = 86, TeamId = t3.TeamId };
            Player player16 = new Player() { PlayerId = 16, Surname = "Asenjo", Firstname = "Sergio", JerseyNumber = 1, DateOfBirth = "1989.01.28", Performance = 84, TeamId = t4.TeamId };
            Player player17 = new Player() { PlayerId = 17, Surname = "Moreno", Firstname = "Alberto", JerseyNumber = 18, DateOfBirth = "1992.07.05", Performance = 67, TeamId = t4.TeamId };
            Player player18 = new Player() { PlayerId = 18, Surname = "Cazorla", Firstname = "Santi", JerseyNumber = 8, DateOfBirth = "1984.12.13", Performance = 75, TeamId = t4.TeamId };
            Player player19 = new Player() { PlayerId = 19, Surname = "Carlos", Firstname = "Bacca", JerseyNumber = 9, DateOfBirth = "1986.09.08", Performance = 81, TeamId = t4.TeamId };
            Player player20 = new Player() { PlayerId = 20, Surname = "Soriano", Firstname = "Bruno", JerseyNumber = 21, DateOfBirth = "1984.06.12", Performance = 77, TeamId = t4.TeamId };
            Player player21 = new Player() { PlayerId = 21, Surname = "Silva", Firstname = "Rui", JerseyNumber = 1, DateOfBirth = "1994.02.07", Performance = 79, TeamId = t5.TeamId };
            Player player22 = new Player() { PlayerId = 22, Surname = "Martínez", Firstname = "Alex", JerseyNumber = 3, DateOfBirth = "1990.08.12", Performance = 68, TeamId = t5.TeamId };
            Player player23 = new Player() { PlayerId = 23, Surname = "Gonalons", Firstname = "Maxime", JerseyNumber = 4, DateOfBirth = "1989.03.10", Performance = 75, TeamId = t5.TeamId };
            Player player24 = new Player() { PlayerId = 24, Surname = "Vico", Firstname = "Fede", JerseyNumber = 14, DateOfBirth = "1994.07.04", Performance = 66, TeamId = t5.TeamId };
            Player player25 = new Player() { PlayerId = 25, Surname = "Soldado", Firstname = "Roberto", JerseyNumber = 9, DateOfBirth = "1985.05.27", Performance = 81, TeamId = t5.TeamId };
            Player player26 = new Player() { PlayerId = 26, Surname = "Remiro", Firstname = "Alex", JerseyNumber = 1, DateOfBirth = "1995.03.24", Performance = 78, TeamId = t6.TeamId };
            Player player27 = new Player() { PlayerId = 27, Surname = "Llorente", Firstname = "Diego", JerseyNumber = 3, DateOfBirth = "1993.08.16", Performance = 88, TeamId = t6.TeamId };
            Player player28 = new Player() { PlayerId = 28, Surname = "Monreal", Firstname = "Nacho", JerseyNumber = 20, DateOfBirth = "1986.02.26", Performance = 81, TeamId = t6.TeamId };
            Player player29 = new Player() { PlayerId = 29, Surname = "Illarramendi", Firstname = "Asier", JerseyNumber = 4, DateOfBirth = "1990.10.22", Performance = 83, TeamId = t6.TeamId };
            Player player30 = new Player() { PlayerId = 30, Surname = "Januzaj", Firstname = "Adnan", JerseyNumber = 20000, DateOfBirth = "1995.02.05", Performance = 81, TeamId = t6.TeamId };
            Player player31 = new Player() { PlayerId = 31, Surname = "Vaclik", Firstname = "Tp,as", JerseyNumber = 1, DateOfBirth = "1989.03.29", Performance = 82, TeamId = t7.TeamId };
            Player player32 = new Player() { PlayerId = 32, Surname = "Navas", Firstname = "Jesús", JerseyNumber = 16, DateOfBirth = "1985.11.21", Performance = 83, TeamId = t7.TeamId };
            Player player33 = new Player() { PlayerId = 33, Surname = "El Haddadi", Firstname = "Munir", JerseyNumber = 11, DateOfBirth = "1995.09.01", Performance = 86, TeamId = t7.TeamId };
            Player player34 = new Player() { PlayerId = 34, Surname = "Lopes", Firstname = "Rony", JerseyNumber = 7, DateOfBirth = "1995.12.28", Performance = 72, TeamId = t7.TeamId };
            Player player35 = new Player() { PlayerId = 35, Surname = "Carlos", Firstname = "Diego", JerseyNumber = 20, DateOfBirth = "1993.03.15", Performance = 71, TeamId = t7.TeamId };
            Player player36 = new Player() { PlayerId = 36, Surname = "Simon", Firstname = "Unai", JerseyNumber = 1, DateOfBirth = "1997.06.11", Performance = 80, TeamId = t8.TeamId };
            Player player37 = new Player() { PlayerId = 37, Surname = "Núnez", Firstname = "Unai", JerseyNumber = 3, DateOfBirth = "1997.01.30", Performance = 84, TeamId = t8.TeamId };
            Player player38 = new Player() { PlayerId = 38, Surname = "García", Firstname = "Dani", JerseyNumber = 14, DateOfBirth = "1990.05.24", Performance = 76, TeamId = t8.TeamId };
            Player player39 = new Player() { PlayerId = 39, Surname = "Muniain", Firstname = "Iker", JerseyNumber = 10, DateOfBirth = "1992.12.19", Performance = 85, TeamId = t8.TeamId };
            Player player40 = new Player() { PlayerId = 40, Surname = "Aduriz", Firstname = "Aritz", JerseyNumber = 20, DateOfBirth = "1981.02.11", Performance = 86, TeamId = t8.TeamId };

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasOne(player => player.Team)
                    .WithMany(team => team.Players)
                    .HasForeignKey(player => player.TeamId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Coach>(entity =>
            {
                entity.HasOne(coach => coach.Player)
                    .WithMany(player => player.Coaches)
                    .HasForeignKey(coach => coach.PlayerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Team>().HasData(t1, t2, t3, t4, t5, t6, t7, t8);
            modelBuilder.Entity<Player>().HasData(player1, player2, player3, player4, player5, player6, player7, player8, player9, player10, player11, player12, player13, player14, player15, player16, player17, player18, player19, player20, player21, player22, player23, player24, player25, player26, player27, player28, player29, player30, player31, player32, player33, player34, player35, player36, player37, player38, player39, player40);
            modelBuilder.Entity<Coach>().HasData(coach1, coach2, coach3, coach4, coach5, coach6, coach7, coach8);
        }
    }
}
