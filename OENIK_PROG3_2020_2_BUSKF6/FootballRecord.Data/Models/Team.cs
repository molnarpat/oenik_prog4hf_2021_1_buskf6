﻿// <copyright file="Team.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    /// <summary>
    /// Team class for team objects.
    /// </summary>
    [Table("Teams")]
    public class Team
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        public Team()
        {
            this.Players = new HashSet<Player>();
        }

        /// <summary>
        /// Gets or sets teamId.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets founded.
        /// </summary>
        public string Founded { get; set; }

        /// <summary>
        /// Gets or sets stadium.
        /// </summary>
        public string Stadium { get; set; }

        /// <summary>
        /// Gets or sets bankroll.
        /// </summary>
        public int BankRoll { get; set; }

        /// <summary>
        /// Gets Players.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Player> Players { get; }

        /// <summary>
        /// ToString method.
        /// </summary>
        /// <returns>The team data to string.</returns>
        public override string ToString()
        {
            return $"ID: [{this.TeamId}] -> Nev: {this.Name} | Alapitasi ev: {this.Founded} | Stadion: {this.Stadium} | Koltsegvetes: {this.BankRoll} ";
        }

        /// <summary>
        /// Compares 2 objects for tests.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns>Boolean that the two object equals.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Team)
            {
                Team other = obj as Team;
                return this.TeamId == other.TeamId;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>Return 0.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
