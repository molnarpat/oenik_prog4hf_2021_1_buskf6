﻿// <copyright file="PlayerListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// PlayerListViewModel.
    /// </summary>
    public class PlayerListViewModel
    {
        /// <summary>
        /// Gets or sets listOfPlayers.
        /// </summary>
        public List<Player> ListOfPlayers { get; set; }

        /// <summary>
        /// Gets or sets editedPlayer.
        /// </summary>
        public Player EditedPlayer { get; set; }
    }
}
