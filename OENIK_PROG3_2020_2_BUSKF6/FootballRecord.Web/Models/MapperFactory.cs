﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// CreateMapper.
        /// </summary>
        /// <returns>config.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Models.Player, Web.Models.Player>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.PlayerId)).
                ForMember(dest => dest.Surname, map => map.MapFrom(src => src.Surname)).
                ForMember(dest => dest.Firstname, map => map.MapFrom(src => src.Firstname)).
                ForMember(dest => dest.JerseyNumber, map => map.MapFrom(src => src.JerseyNumber)).
                ForMember(dest => dest.DateOfBirth, map => map.MapFrom(src => src.DateOfBirth)).
                ForMember(dest => dest.Performance, map => map.MapFrom(src => src.Performance));
            });
            return config.CreateMapper();
        }
    }
}
