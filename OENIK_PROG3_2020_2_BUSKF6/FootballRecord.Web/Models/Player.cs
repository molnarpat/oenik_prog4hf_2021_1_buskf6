﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Player class.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets player Id.
        /// </summary>
        [Display(Name = "Player Id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets Surname.
        /// </summary>
        [Display(Name = "Player Surname")]
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets player Firstname.
        /// </summary>
        [Display(Name = "Player Firstname")]
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets player JerseyNumber.
        /// </summary>
        [Display(Name = "Player Jersey number")]
        [Required]
        public int JerseyNumber { get; set; }

       /// <summary>
       /// Gets or sets player DateOfbirth.
       /// </summary>
        [Display(Name = "Player Date Of Birth")]
        [Required]
        public string DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets player performance.
        /// </summary>
        [Display(Name = "Player Performance")]
        [Required]
        public int Performance { get; set; }
    }
}
