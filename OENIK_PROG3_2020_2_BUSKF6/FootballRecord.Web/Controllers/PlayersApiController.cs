﻿// <copyright file="PlayersApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballRecord.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The PlayersApiController class.
    /// </summary>
    public class PlayersApiController : Controller
    {
        private IPlayerLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersApiController"/> class.
        /// </summary>
        /// <param name="logic">Logic.</param>
        /// <param name="mapper">Mapper.</param>
        public PlayersApiController(IPlayerLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// GetAll method.
        /// </summary>
        /// <returns>All players.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Player> GetAll()
        {
            var players = logic.GetAllPlayers();
            return mapper.Map<IList<Data.Models.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Delete one player.
        /// </summary>
        /// <param name="id">Player's id.</param>
        /// <returns>Delete the player.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOnePlayer(int id)
        {
            return new ApiResult() { OperationResult = logic.DeletePlayer(id) };
        }

        /// <summary>
        /// Add one car.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <returns>Added a car.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePlayer(Models.Player player)
        {
            bool success = true;
            try
            {
                logic.AddPlayer(player.Surname, player.Firstname, player.JerseyNumber, player.DateOfBirth, player.Performance, 1);
            }
            catch (Exception)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modify one player.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <returns>Modify.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOnePlayer(Models.Player player)
        {
            return new ApiResult() { OperationResult = logic.UpdatePlayer(player.Id, player.Surname, player.Firstname, player.JerseyNumber, player.DateOfBirth, player.Performance) };
        }
    }
}
