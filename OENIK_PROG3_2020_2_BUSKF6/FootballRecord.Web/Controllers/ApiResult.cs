﻿// <copyright file="ApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Controllers
{
    /// <summary>
    /// ApiResult class.
    /// </summary>
        public class ApiResult
        {
            /// <summary>
            /// Gets or sets a value indicating whether operationresult.
            /// </summary>
            public bool OperationResult { get; set; }
        }
}
