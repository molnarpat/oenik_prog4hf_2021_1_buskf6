﻿// <copyright file="PlayersController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballRecord.Logic;
    using FootballRecord.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// PlayersController.
    /// </summary>
    public class PlayersController : Controller
    {
        private IPlayerLogic logic;
        private IMapper mapper;
        private PlayerListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersController"/> class.
        /// PlayersController constructor.
        /// </summary>
        /// <param name="logic">Logic.</param>
        /// <param name="mapper">Mapper.</param>
        public PlayersController(IPlayerLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            vm = new PlayerListViewModel();
            vm.EditedPlayer = new Models.Player();

            var players = logic.GetAllPlayers();
            vm.ListOfPlayers = mapper.Map<IList<Data.Models.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Index.
        /// </summary>
        /// <returns>View.</returns>
        public IActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("PlayersIndex", vm);
        }

        private Models.Player GetPlayerModel(int id)
        {
            Data.Models.Player onePlayer = logic.GetPlayerById(id);
            return mapper.Map<Data.Models.Player, Models.Player>(onePlayer);
        }

        /// <summary>
        /// Details.
        /// </summary>
        /// <param name="id">Player Id.</param>
        /// <returns>View.</returns>
        public IActionResult Details(int id)
        {
            return View("PlayersDetails", GetPlayerModel(id));
        }

        /// <summary>
        /// Remove.
        /// </summary>
        /// <param name="id">Player Id.</param>
        /// <returns>View.</returns>
        public IActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";

            if (logic.DeletePlayer(id))
            {
                TempData["editResult"] = "Delete OK";
            }

            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="id">Player Id.</param>
        /// <returns>View.</returns>
        public IActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedPlayer = GetPlayerModel(id);
            return View("PlayersIndex", vm);
        }

        /// <summary>
        /// Edit.
        /// </summary>
        /// <param name="player">Model Player.</param>
        /// <param name="editAction">EditAction.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Player player, string editAction)
        {
            if (ModelState.IsValid && player != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        logic.AddPlayer(player.Surname, player.Firstname, player.JerseyNumber, player.DateOfBirth, player.Performance, 1);
                    }
                    catch (ArgumentException ex)
                    {
                        TempData["editResult"] = "AddPlayer FAIL: " + ex.Message;
                    }
                }
                else
                {
                    if (!logic.UpdatePlayer(player.Id, player.Surname, player.Firstname, player.JerseyNumber, player.DateOfBirth, player.Performance))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedPlayer = player;
                return View("PlayersIndex", vm);
            }
        }
    }
}
