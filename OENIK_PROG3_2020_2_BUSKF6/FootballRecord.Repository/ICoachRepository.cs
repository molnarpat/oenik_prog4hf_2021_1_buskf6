﻿// <copyright file="ICoachRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the coach repository.
    /// </summary>
    public interface ICoachRepository : IRepository<Coach>
    {
        /// <summary>
        /// Inserts a coach to the database.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="salary">salary.</param>
        /// <param name="nationality">nationality.</param>
        /// <param name="addperformance">Addperformance.</param>
        /// <param name="age">Age.</param>
        /// <param name="playerid">Playerid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool CreateCoach(string name, int salary, string nationality, int addperformance, int age, int playerid);

        /// <summary>
        /// Updates a coaches name in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="name">Name.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateName(int id, string name);

        /// <summary>
        /// Updates a coaches salary in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="salary">Salary.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateSalary(int id, int salary);

        /// <summary>
        /// Updates a coaches nationality in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="nationality">Nationality.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateNationality(int id, string nationality);

        /// <summary>
        /// Updates a coaches addperformance in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="addperformance">Addperformance.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateAddPerformance(int id, int addperformance);

        /// <summary>
        /// Updates a coaches age in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="age">Age.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateAge(int id, int age);

        /// <summary>
        /// Delete a coach from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeleteCoach(int id);
    }
}
