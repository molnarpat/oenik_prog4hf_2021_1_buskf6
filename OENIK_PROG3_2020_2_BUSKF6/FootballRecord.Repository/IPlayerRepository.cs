﻿// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the player repository.
    /// </summary>
    public interface IPlayerRepository : IRepository<Player>
    {
        /// <summary>
        /// Inserts a player to the database.
        /// </summary>
        /// <param name="surname">surname. </param>
        /// <param name="firstname">firstname. </param>
        /// <param name="jerseynumber">jerseynumber. </param>
        /// <param name="dateofbirth">dateofbirth. </param>
        /// <param name="performance">performance. </param>
        /// <param name="teamid">Teamid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool CreatePlayer(string surname, string firstname, int jerseynumber, string dateofbirth, int performance, int teamid);

        /// <summary>
        /// Updates a players date of birts in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="dateofbirth">Date of birth. </param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateDateOfBirth(int id, string dateofbirth);

        /// <summary>
        /// Updates a players jersey number in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="jerseynumber">Jersey number.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateJerseyNumber(int id, int jerseynumber);

        /// <summary>
        /// Updates a players surname in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="surname">Surname.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateSurname(int id, string surname);

        /// <summary>
        /// Updates a players firstname in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="firstname">Firstname.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateFirstname(int id, string firstname);

        /// <summary>
        /// Updates a players performance in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="performance">Performance.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdatePerformance(int id, int performance);

        /// <summary>
        /// Delete a player from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeletePlayer(int id);

        /// <summary>
        /// Update Player from the database.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="surname">Surname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="jerseynumber">Jersey Number.</param>
        /// <param name="dateofbirth">Date of Birth.</param>
        /// <param name="performance">Performance.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdatePlayer(int id, string surname, string firstname, int jerseynumber, string dateofbirth, int performance);
    }
}
