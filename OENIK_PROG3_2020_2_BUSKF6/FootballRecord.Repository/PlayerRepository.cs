﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The players repository layer class.
    /// This accesses the database.
    /// </summary>
    public class PlayerRepository : IPlayerRepository
    {
        private FootballDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRepository"/> class.
        /// </summary>
        /// <param name="db">Database.</param>
        public PlayerRepository(FootballDBContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a player to the database.
        /// </summary>
        /// <param name="surname">Surname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="jerseynumber">Jersey number.</param>
        /// <param name="dateofbirth">Date of birth.</param>
        /// <param name="performance">Performance.</param>
        /// <param name="teamid">teamid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        public bool CreatePlayer(string surname, string firstname, int jerseynumber, string dateofbirth, int performance, int teamid)
        {
            try
            {
                this.db.Players.Add(new Player() { Surname = surname, Firstname = firstname, JerseyNumber = jerseynumber, DateOfBirth = dateofbirth, Performance = performance, TeamId = teamid });
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete a player from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeletePlayer(int id)
        {
            try
            {
                this.db.Players.Remove(this.GetById(id));
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets all the players from the database.
        /// </summary>
        /// <returns>Returns all the player records as a list.</returns>
        public IQueryable<Player> GetAll()
        {
            return this.db.Set<Player>();
        }

        /// <summary>
        /// Gets a player by id.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a player as a class.</returns>
        public Player GetById(int id)
        {
            try
            {
                return this.db.Players.Where(x => x.PlayerId == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Updates a players date of birts in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="dateofbirth">Date of birth. </param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateDateOfBirth(int id, string dateofbirth)
        {
            try
            {
                var player = this.GetById(id);
                player.DateOfBirth = dateofbirth;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a players firstname in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="firstname">Firstname.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateFirstname(int id, string firstname)
        {
            try
            {
                var player = this.GetById(id);
                player.Firstname = firstname;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a players jersey number in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="jerseynumber">Jersey number.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateJerseyNumber(int id, int jerseynumber)
        {
            try
            {
                var player = this.GetById(id);
                player.JerseyNumber = jerseynumber;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a players performance in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="performance">Performance.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdatePerformance(int id, int performance)
        {
            try
            {
                var player = this.GetById(id);
                player.Performance = performance;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a players surname in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="surname">Surname.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateSurname(int id, string surname)
        {
            try
            {
                var player = this.GetById(id);
                player.Surname = surname;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Update player in the database.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="surname">Surname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="jerseynumber">Jersey Number.</param>
        /// <param name="dateofbirth">Date Of Birth.</param>
        /// <param name="performance">Performance.</param>
        /// <returns>Returns a boolean that tells the update was succesful.</returns>
        public bool UpdatePlayer(int id, string surname, string firstname, int jerseynumber, string dateofbirth, int performance)
        {
            try
            {
                var player = this.GetById(id);

                player.Surname = surname;
                player.Firstname = firstname;
                player.JerseyNumber = jerseynumber;
                player.DateOfBirth = dateofbirth;
                player.Performance = performance;
                this.db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
