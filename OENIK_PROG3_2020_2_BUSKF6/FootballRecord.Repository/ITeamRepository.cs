﻿// <copyright file="ITeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the team repository.
    /// </summary>
    public interface ITeamRepository : IRepository<Team>
    {
        /// <summary>
        /// Inserts a team to the database.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="founded">Founded.</param>
        /// <param name="stadium">Stadium.</param>
        /// <param name="bankroll">Bankroll.</param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool CreateTeam(string name, string founded, string stadium, int bankroll);

        /// <summary>
        /// Updates a teams name in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="name">Name.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateName(int id, string name);

        /// <summary>
        /// Updates a teams founded in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="year">Founded date.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateFounded(int id, string year);

        /// <summary>
        /// Updates a teams stadium in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="stadium">Stadium.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateStadium(int id, string stadium);

        /// <summary>
        /// Updates a teams bankroll in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="bankroll">Bankroll.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        bool UpdateBankRoll(int id, int bankroll);

        /// <summary>
        /// Delete a team from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeleteTeam(int id);
    }
}
