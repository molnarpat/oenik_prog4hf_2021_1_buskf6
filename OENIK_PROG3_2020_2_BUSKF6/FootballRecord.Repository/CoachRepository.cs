﻿// <copyright file="CoachRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The coach repository layer class.
    /// This accesses the database.
    /// </summary>
    public class CoachRepository : ICoachRepository
    {
        private FootballDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CoachRepository"/> class.
        /// </summary>
        /// <param name="db">Database.</param>
        public CoachRepository(FootballDBContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a coach to the database.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="salary">salary.</param>
        /// <param name="nationality">nationality.</param>
        /// <param name="addperformance">Addperformance.</param>
        /// <param name="age">Age.</param>
        /// <param name="playerid">Playerid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        public bool CreateCoach(string name, int salary, string nationality, int addperformance, int age, int playerid)
        {
            try
            {
                this.db.Coaches.Add(new Coach() { Name = name, Salary = salary, Nationality = nationality, AddPerformance = addperformance, Age = age, PlayerId = playerid });
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete a coach from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeleteCoach(int id)
        {
            try
            {
                this.db.Coaches.Remove(this.GetById(id));
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets all the coaches from the database.
        /// </summary>
        /// <returns>Returns all the coach records as a list.</returns>
        public IQueryable<Coach> GetAll()
        {
            return this.db.Set<Coach>();
        }

        /// <summary>
        /// Gets a coach by id.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a coach as a class.</returns>
        public Coach GetById(int id)
        {
            try
            {
                return this.db.Coaches.Where(x => x.CoachId == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Updates a coaches addperformance in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="addperformance">Addperformance.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateAddPerformance(int id, int addperformance)
        {
            try
            {
                var coach = this.GetById(id);
                coach.AddPerformance = addperformance;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a coaches age in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="age">Age.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateAge(int id, int age)
        {
            try
            {
                var coach = this.GetById(id);
                coach.Age = age;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a coaches name in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="name">Name.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateName(int id, string name)
        {
            try
            {
                var coach = this.GetById(id);
                coach.Name = name;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a coaches nationality in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="nationality">Nationality.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateNationality(int id, string nationality)
        {
            try
            {
                var coach = this.GetById(id);
                coach.Nationality = nationality;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a coaches salary in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="salary">Salary.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateSalary(int id, int salary)
        {
            try
            {
                var coach = this.GetById(id);
                coach.Salary = salary;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
