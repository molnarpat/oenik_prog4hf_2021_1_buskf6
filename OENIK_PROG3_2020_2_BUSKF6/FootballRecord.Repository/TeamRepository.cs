﻿// <copyright file="TeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The team repository layer class.
    /// This accesses the database.
    /// </summary>
    public class TeamRepository : ITeamRepository
    {
        private FootballDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamRepository"/> class.
        /// </summary>
        /// <param name="db">Database.</param>
        public TeamRepository(FootballDBContext db)
        {
            this.db = db;
        }

        /// <summary>
        /// Inserts a team to the database.
        /// </summary>
        /// <param name="name">Name.</param>
        /// <param name="founded">Founded.</param>
        /// <param name="stadium">Stadium.</param>
        /// <param name="bankroll">Bankroll.</param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        public bool CreateTeam(string name, string founded, string stadium, int bankroll)
        {
            try
            {
                this.db.Teams.Add(new Team() { Name = name, Founded = founded, Stadium = stadium, BankRoll = bankroll });
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete a team from the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeleteTeam(int id)
        {
            try
            {
                this.db.Teams.Remove(this.GetById(id));
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets all the teams from the database.
        /// </summary>
        /// <returns>Returns all the team records as a list.</returns>
        public IQueryable<Team> GetAll()
        {
            return this.db.Set<Team>();
        }

        /// <summary>
        /// Gets a team by id.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a team as a class.</returns>
        public Team GetById(int id)
        {
            try
            {
                return this.db.Teams.Where(x => x.TeamId == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Updates a teams bankroll in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="bankroll">Bankroll.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateBankRoll(int id, int bankroll)
        {
            try
            {
                var team = this.GetById(id);
                team.BankRoll = bankroll;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a teams founded in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="year">Founded date.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateFounded(int id, string year)
        {
            try
            {
                var team = this.GetById(id);
                team.Founded = year;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a teams name in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="name">Name.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateName(int id, string name)
        {
            try
            {
                var team = this.GetById(id);
                team.Name = name;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a teams stadium in the database.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="stadium">Stadium.</param>
        /// <returns>Returns a boolean that tells the update was successful.</returns>
        public bool UpdateStadium(int id, string stadium)
        {
            try
            {
                var team = this.GetById(id);
                team.Stadium = stadium;
                this.db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
