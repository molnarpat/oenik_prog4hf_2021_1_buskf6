﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The general repository interface.
    /// Get one and get all functions.
    /// </summary>
    /// <typeparam name="T">A Table type.</typeparam>
    public interface IRepository<T>

       where T : class
    {
        /// <summary>
        /// Gets a general instance by id.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns the chosen instance.</returns>
        T GetById(int id);

        /// <summary>
        /// Gets all the instances from the databease.
        /// </summary>
        /// <returns>Returns all the footballrecord records as a list.</returns>
        IQueryable<T> GetAll();
    }
}
