﻿// <copyright file="ICoachLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the Coach logic layer.
    /// </summary>
    public interface ICoachLogic
    {
        /// <summary>
        /// Calls the repo layer to get all the coaches and collects its ids.
        /// </summary>
        /// <returns>List of all current coaches ids. </returns>
        IList<int> GetCurrentCoachIDs();

        /// <summary>
        /// Calls the repository layer to add a coach.
        /// </summary>
        /// <param name="name">name. </param>
        /// <param name="salary">salary. </param>
        /// <param name="nationality">nationality. </param>
        /// <param name="addperformance">addperformance. </param>
        /// <param name="age">age. </param>
        /// <param name="playerid">Playerid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool AddCoach(string name, int salary, string nationality, int addperformance, int age, int playerid);

        /// <summary>
        /// Calls the repo layer to get a coach record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a coach by id. </returns>
        Coach GetCoachById(int id);

        /// <summary>
        /// Calls the repo layer to get all the coach records.
        /// </summary>
        /// <returns>A list that has all the coaches. </returns>
        IList<Coach> GetAllCoaches();

        /// <summary>
        /// Calls the repo layer to change the salary of a coach.
        /// </summary>
        /// <param name="id">id. </param>
        /// <param name="newsalary">newsalary. </param>
        /// <returns>Returns a boolean that tells the change was successful.</returns>
        bool ChangeCoachSalary(int id, int newsalary);

        /// <summary>
        ///  Calls the repo layer to delete a coach by id.
        /// </summary>
        /// <param name="id">id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeleteCoach(int id);
    }
}
