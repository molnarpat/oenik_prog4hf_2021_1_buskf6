﻿// <copyright file="ITeamLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the Team logic layer.
    /// </summary>
    public interface ITeamLogic
    {
        /// <summary>
        /// Calls the repo layer to get all the teams and collects its ids.
        /// </summary>
        /// <returns>List of all current team ids. </returns>
        IList<int> GetCurrentTeamIDs();

        /// <summary>
        /// Calls the repository layer to add a team.
        /// </summary>
        /// <param name="name">name. </param>
        /// <param name="founded">founded. </param>
        /// <param name="stadium">stadium. </param>
        /// <param name="bankroll">bankroll. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool AddTeam(string name, string founded, string stadium, int bankroll);

        /// <summary>
        /// Calls the repo layer to get a team record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a team by id. </returns>
        Team GetTeamById(int id);

        /// <summary>
        /// Calls the repo layer to get all the team records.
        /// </summary>
        /// <returns>A list that has all the teams. </returns>
        IList<Team> GetAllTeams();

        /// <summary>
        /// Calls the repo layer to change the bankroll of a team.
        /// </summary>
        /// <param name="id">id. </param>
        /// <param name="newbankroll">newbankroll. </param>
        /// <returns>Returns a boolean that tells the change was successful. </returns>
        bool ChangeTeamBankRoll(int id, int newbankroll);

        /// <summary>
        ///  Calls the repo layer to delete a team by id.
        /// </summary>
        /// <param name="id">id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeleteTeam(int id);

        /// <summary>
        /// Calls the repo layer to get the youngest team.
        /// </summary>
        /// <returns>Return list of teams who have a higher average performance players.</returns>
        IList<Team> GetYoungestTeam();

        /// <summary>
        /// Calls the repo layer to get the youngest team async.
        /// </summary>
        /// <returns>Return list of teams who have a youngest players.</returns>
        Task<IList<Team>> GetYoungestTeamAsync();

        /// <summary>
        /// Calls the repo layer to get all the average performance.
        /// </summary>
        /// <returns>Return list of teams who have a best performance players.</returns>
        IList<AveragesResult> GetAveragePerformance();

        /// <summary>
        /// Calls the repo layer to get all the average performance async.
        /// </summary>
        /// <returns>Return list of teams who have a best performance players.</returns>
        Task<IList<AveragesResult>> GetAveragePerformanceAsync();
    }
}
