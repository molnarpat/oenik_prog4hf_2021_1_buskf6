﻿// <copyright file="CoachLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballRecord.Data.Models;
    using FootballRecord.Repository;

    /// <summary>
    /// Coach logic class.
    /// </summary>
    public class CoachLogic : ICoachLogic
    {
        private ICoachRepository coachRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CoachLogic"/> class.
        /// </summary>
        /// <param name="coachrepo">coachrepo.</param>
        public CoachLogic(ICoachRepository coachrepo)
        {
            this.coachRepo = coachrepo;
        }

        /// <summary>
        /// Calls the repository to add a coach.
        /// </summary>
        /// <param name="name">Name. </param>
        /// <param name="salary">Salary. </param>
        /// <param name="nationality">Nationality. </param>
        /// <param name="addperformance">Addperformance. </param>
        /// <param name="age">Age. </param>
        /// <param name="playerid">Playerid. </param>
        /// <returns>Returns a boolean that tells the addition was successful. </returns>
        public bool AddCoach(string name, int salary, string nationality, int addperformance, int age, int playerid)
        {
            return this.coachRepo.CreateCoach(name, salary, nationality, addperformance, age, playerid);
        }

        /// <summary>
        /// Calls the repository to change a coach salary.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="newsalary">New salary.</param>
        /// <returns>Returns a boolean that tells the change was successful. </returns>
        public bool ChangeCoachSalary(int id, int newsalary)
        {
            return this.coachRepo.UpdateSalary(id, newsalary);
        }

        /// <summary>
        /// Calls the repository to delete a coach.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeleteCoach(int id)
        {
            return this.coachRepo.DeleteCoach(id);
        }

        /// <summary>
        /// Calls the repository to get all the coach records.
        /// </summary>
        /// <returns>A list that has all the coaches. </returns>
        public IList<Coach> GetAllCoaches()
        {
           return this.coachRepo.GetAll().ToList();
        }

        /// <summary>
        /// Calls the repository to get a coach record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a coach by id. </returns>
        public Coach GetCoachById(int id)
        {
            return this.coachRepo.GetById(id);
        }

        /// <summary>
        /// Calls the repository to get all the coaches and collects its ids.
        /// </summary>
        /// <returns>List of all current coaches ids. </returns>
        public IList<int> GetCurrentCoachIDs()
        {
            return this.GetAllCoaches().Select(x => x.CoachId).ToList();
        }
    }
}
