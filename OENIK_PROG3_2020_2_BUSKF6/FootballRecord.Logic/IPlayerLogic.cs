﻿// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Data.Models;

    /// <summary>
    /// The interface of the Player logic layer.
    /// </summary>
    public interface IPlayerLogic
    {
        /// <summary>
        /// Calls the repo layer to get all the players and collects its ids.
        /// </summary>
        /// <returns>List of all current player ids. </returns>
        IList<int> GetCurrentPlayerIDs();

        /// <summary>
        /// Calls the repository layer to add a player.
        /// </summary>
        /// <param name="surname">surname. </param>
        /// <param name="firstname">firstname. </param>
        /// <param name="jerseynumber">jerseynumber. </param>
        /// <param name="dateofbirth">dateofbirth. </param>
        /// <param name="performance">performance. </param>
        /// <param name="teamid">Teamid. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        bool AddPlayer(string surname, string firstname, int jerseynumber, string dateofbirth, int performance, int teamid);

        /// <summary>
        /// Calls the repo layer to get a player record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a player by id. </returns>
        Player GetPlayerById(int id);

        /// <summary>
        /// Calls the repo layer to get all the player records.
        /// </summary>
        /// <returns>A list that has all the players. </returns>
        IList<Player> GetAllPlayers();

        /// <summary>
        /// Calls the repo layer to change the performance of a player.
        /// </summary>
        /// <param name="id">id. </param>
        /// <param name="newperformance">newperformance. </param>
        /// <returns>Returns a boolean that tells the change was successful.</returns>
        bool ChangePlayerPerformance(int id, int newperformance);

        /// <summary>
        ///  Calls the repo layer to delete a player by id.
        /// </summary>
        /// <param name="id">id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        bool DeletePlayer(int id);

        /// <summary>
        /// Calls the repo layer to get all the players who have a coach.
        /// </summary>
        /// <returns>List of players who have a coach. </returns>
        IList<Player> GetPlayersWithCoach();

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team.
        /// </summary>
        /// <returns>The lowest bankroll team.</returns>
        IList<Player> GetPlayersLowestBankRollTeam();

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team async.
        /// </summary>
        /// <returns>The lowest bankroll team.</returns>
        Task<IList<Player>> GetPlayersLowestBankRollTeamAsync();

        /// <summary>
        /// Calls the Repo layer to update player.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="surname">Surname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="jerseynumber">Jersey Number.</param>
        /// <param name="dateofbirth">Date of Birth.</param>
        /// <param name="performance">Performance.</param>
        /// <returns>Boolean that tells the update was succesful.</returns>
        bool UpdatePlayer(int id, string surname, string firstname, int jerseynumber, string dateofbirth, int performance);
    }
}
