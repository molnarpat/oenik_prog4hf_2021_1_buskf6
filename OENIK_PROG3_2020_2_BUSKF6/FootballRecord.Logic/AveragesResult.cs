﻿// <copyright file="AveragesResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Assistant class to calculate the average.
    /// </summary>
    public class AveragesResult
    {
        /// <summary>
        /// Gets or sets TeamName.
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Gets or sets AveragePerformance.
        /// </summary>
        public double AveragePerformance { get; set; }

        /// <summary>
        /// Override tostring.
        /// </summary>
        /// <returns>Teamname, Averageperformance.</returns>
        public override string ToString()
        {
            return $"Csapat = {this.TeamName}, Atlag = {this.AveragePerformance}";
        }

        /// <summary>
        /// Compares 2 objects.
        /// </summary>
        /// <param name="obj">obj.</param>
        /// <returns>Boolean that the two object equals.</returns>
        public override bool Equals(object obj)
        {
            if (obj is AveragesResult)
            {
                AveragesResult other = obj as AveragesResult;
                return this.TeamName == other.TeamName &&
                    this.AveragePerformance == other.AveragePerformance;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Override GetHashCode.
        /// </summary>
        /// <returns>Return 0.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
