﻿// <copyright file="TeamLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Data.Models;
    using FootballRecord.Repository;

    /// <summary>
    /// Team logic class.
    /// </summary>
    public class TeamLogic : ITeamLogic
    {
        private IPlayerRepository playerRepo;
        private ITeamRepository teamRepo;
        private ICoachRepository coachRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TeamLogic"/> class.
        /// </summary>
        /// <param name="playerrepo">playerrepo. </param>
        /// <param name="teamrepo">teamrepo. </param>
        /// <param name="coachrepo">coachrepo. </param>
        public TeamLogic(IPlayerRepository playerrepo, ITeamRepository teamrepo, ICoachRepository coachrepo)
        {
            this.playerRepo = playerrepo;
            this.teamRepo = teamrepo;
            this.coachRepo = coachrepo;
        }

        /// <summary>
        /// Calls the repository to add a team.
        /// </summary>
        /// <param name="name">Name. </param>
        /// <param name="founded">Founded. </param>
        /// <param name="stadium">Stadium. </param>
        /// <param name="bankroll">Bankroll. </param>
        /// <returns>Returns a boolean that tells the addition was successful.</returns>
        public bool AddTeam(string name, string founded, string stadium, int bankroll)
        {
            int teamId = this.GetAllTeams().Max(x => x.TeamId) + 1;
            return this.teamRepo.CreateTeam(name, founded, stadium, bankroll);
        }

        /// <summary>
        /// Calls the repository to change a team bankroll.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="newbankroll">New bankroll. </param>
        /// <returns>Returns a boolean that tells the change was successful.</returns>
        public bool ChangeTeamBankRoll(int id, int newbankroll)
        {
            return this.teamRepo.UpdateBankRoll(id, newbankroll);
        }

        /// <summary>
        /// Calls the repository to delete a team.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeleteTeam(int id)
        {
            return this.teamRepo.DeleteTeam(id);
        }

        /// <summary>
        /// Calls the repository to get all the team records.
        /// </summary>
        /// <returns>A list that has all the teams. </returns>
        public IList<Team> GetAllTeams()
        {
            List<Team> teams = this.teamRepo.GetAll().ToList();
            List<Coach> coaches = this.coachRepo.GetAll().ToList();
            List<Player> players = this.playerRepo.GetAll().ToList();
            List<Team> results = new List<Team>();
            foreach (var team in teams)
            {
                int bankroll = team.BankRoll;
                foreach (var player in players.Where(x => x.TeamId == team.TeamId))
                {
                    foreach (var coach in coaches.Where(x => x.PlayerId == player.PlayerId))
                    {
                        bankroll -= coach.Salary;
                    }
                }

                results.Add(new Team()
                {
                    TeamId = team.TeamId,
                    Name = team.Name,
                    Founded = team.Founded,
                    Stadium = team.Stadium,
                    BankRoll = bankroll,
                });
            }

            return results;
        }

        /// <summary>
        /// Calls the repository to get all the teams and collects its ids.
        /// </summary>
        /// <returns>List of all current team ids. </returns>
        public IList<int> GetCurrentTeamIDs()
        {
            return this.GetAllTeams().Select(x => x.TeamId).ToList();
        }

        /// <summary>
        /// Calls the repository to get a team record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a team by id. </returns>
        public Team GetTeamById(int id)
        {
            Team team = this.teamRepo.GetById(id);
            List<Player> players = this.playerRepo.GetAll().ToList();
            List<Coach> coaches = this.coachRepo.GetAll().ToList();
            int bankroll = team.BankRoll;
            foreach (var player in players.Where(x => x.TeamId == team.TeamId))
            {
                foreach (var coach in coaches.Where(x => x.PlayerId == player.PlayerId))
                {
                    bankroll -= coach.Salary;
                }
            }

            return new Team()
            {
                TeamId = team.TeamId,
                Name = team.Name,
                Founded = team.Founded,
                Stadium = team.Stadium,
                BankRoll = bankroll,
            };
        }

        /// <summary>
        /// Calls the repo layer to get all the average performance.
        /// </summary>
        /// <returns>Return list of teams who have a best performance players.</returns>
        public IList<AveragesResult> GetAveragePerformance()
        {
            List<Team> teams = this.teamRepo.GetAll().ToList();
            List<Player> players = this.playerRepo.GetAll().ToList();

            var q = from player in players
                        group player by new { player.Team.TeamId, player.Team.Name } into grp
                        select new AveragesResult
                        {
                            TeamName = grp.Key.Name,
                            AveragePerformance = grp.Average(player => player.Performance),
                        };

            return q.ToList();
        }

        /// <summary>
        /// Calls the repo layer to get the youngest team.
        /// </summary>
        /// <returns>Return list of teams who have a youngest players..</returns>
        public IList<Team> GetYoungestTeam()
        {
            List<Team> teams = this.teamRepo.GetAll().ToList();
            List<Player> players = this.playerRepo.GetAll().ToList();

            long nowTicks = DateTime.Now.Ticks;

            var q1 =
                from team in teams
                join player in players on team.TeamId equals player.TeamId
                select new
                {
                    PlayerAge = new DateTime(nowTicks - DateTime.Parse(player.DateOfBirth).Ticks).Year,
                    TeamID = team.TeamId,
                };
            var q2 =
                        from player2 in q1
                        group player2 by new { player2.TeamID } into grp
                        select new
                        {
                            TeamID2 = grp.Key.TeamID,
                            AvgAge = grp.Average(x => x.PlayerAge),
                        };

            var q3 =
                from team2 in teams
                join result in q2
                on team2.TeamId equals result.TeamID2
                orderby result.AvgAge ascending
                select team2;

            return q3.ToList();
        }

        /// <summary>
        /// Calls the repo layer to get the youngest team async.
        /// </summary>
        /// <returns>Return list of teams who have a higher average performance players.</returns>
        public Task<IList<Team>> GetYoungestTeamAsync()
        {
            return Task.Run(() => this.GetYoungestTeam());
        }

        /// <summary>
        /// Calls the repo layer to get all the average performance async.
        /// </summary>
        /// <returns>Return list of teams who have a best performance players.</returns>
        public Task<IList<AveragesResult>> GetAveragePerformanceAsync()
        {
            return Task.Run(() => this.GetAveragePerformance());
        }
    }
}
