﻿// <copyright file="PlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballRecord.Data.Models;
    using FootballRecord.Repository;

    /// <summary>
    /// Player logic class.
    /// </summary>
    public class PlayerLogic : IPlayerLogic
    {
        private IPlayerRepository playerRepo;
        private ICoachRepository coachRepo;
        private ITeamRepository teamRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerLogic"/> class.
        /// </summary>
        /// <param name="playerrepo">playerrepo.</param>
        /// <param name="coachrepo">coachrepo.</param>
        /// <param name="teamrepo">teamrepo. </param>
        public PlayerLogic(IPlayerRepository playerrepo, ICoachRepository coachrepo, ITeamRepository teamrepo)
        {
            this.playerRepo = playerrepo;
            this.coachRepo = coachrepo;
            this.teamRepo = teamrepo;
        }

        /// <summary>
        /// Calls the repository to add a player.
        /// </summary>
        /// <param name="surname">Surname. </param>
        /// <param name="firstname">Firstname. </param>
        /// <param name="jerseynumber">Jersey number.</param>
        /// <param name="dateofbirth">Date of birth. </param>
        /// <param name="performance">Performance. </param>
        /// <param name="teamid">Param. </param>
        /// <returns>Returns a boolean that tells the addition was successful. </returns>
        public bool AddPlayer(string surname, string firstname, int jerseynumber, string dateofbirth, int performance, int teamid)
        {
            return this.playerRepo.CreatePlayer(surname, firstname, jerseynumber, dateofbirth, performance, teamid);
        }

        /// <summary>
        /// Calls the repository to change a player performance.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <param name="newperformance">New performance. </param>
        /// <returns>Returns a boolean that tells the change was successful. </returns>
        public bool ChangePlayerPerformance(int id, int newperformance)
        {
            return this.playerRepo.UpdatePerformance(id, newperformance);
        }

        /// <summary>
        /// Calls the repository to delete a player.
        /// </summary>
        /// <param name="id">Id. </param>
        /// <returns>Returns a boolean that tells the delete was successful.</returns>
        public bool DeletePlayer(int id)
        {
            return this.playerRepo.DeletePlayer(id);
        }

        /// <summary>
        /// Calls the repository to get all the player records.
        /// </summary>
        /// <returns>A list that has all the players. </returns>
        public IList<Player> GetAllPlayers()
        {
            List<Player> players = this.playerRepo.GetAll().ToList();
            List<Coach> coaches = this.coachRepo.GetAll().ToList();
            List<Player> results = new List<Player>();
            foreach (var player in players)
            {
                int playerperformance = player.Performance;
                foreach (var coach in coaches.Where(x => x.PlayerId == player.PlayerId))
                {
                    playerperformance += coach.AddPerformance;
                    if (playerperformance > 100)
                    {
                        playerperformance = 100;
                        break;
                    }
                }

                results.Add(new Player()
                {
                    PlayerId = player.PlayerId,
                    Surname = player.Surname,
                    Firstname = player.Firstname,
                    JerseyNumber = player.JerseyNumber,
                    DateOfBirth = player.DateOfBirth,
                    Performance = playerperformance,
                });
            }

            return results;
        }

        /// <summary>
        /// Calls the repository to get all the players and collects its ids.
        /// </summary>
        /// <returns>List of all current player ids. </returns>
        public IList<int> GetCurrentPlayerIDs()
        {
            return this.GetAllPlayers().Select(x => x.PlayerId).ToList();
        }

        /// <summary>
        /// Calls the repository to get a player record.
        /// </summary>
        /// <param name="id"> id. </param>
        /// <returns>Returns a player by id. </returns>
        public Player GetPlayerById(int id)
        {
            Player player = this.playerRepo.GetById(id);
            if (player == null)
            {
                return null;
            }

            List<Coach> coaches = this.coachRepo.GetAll().ToList();

            int playerperformance = player.Performance;
            foreach (var coach in coaches.Where(x => x.PlayerId == player.PlayerId))
            {
                playerperformance += coach.AddPerformance;
                if (playerperformance > 100)
                {
                    playerperformance = 100;
                    break;
                }
            }

            return new Player()
            {
                PlayerId = player.PlayerId,
                Surname = player.Surname,
                Firstname = player.Firstname,
                JerseyNumber = player.JerseyNumber,
                DateOfBirth = player.DateOfBirth,
                Performance = playerperformance,
            };
        }

        /// <summary>
        /// Calls the repository to get all the players who have a coach.
        /// </summary>
        /// <returns>List of players who have a coach. </returns>
        public IList<Player> GetPlayersWithCoach()
        {
            List<Coach> coaches = this.coachRepo.GetAll().ToList();
            List<Player> players = this.playerRepo.GetAll().ToList();

            var playerswithcoach = from c in coaches
                                   join p in players on c.PlayerId equals p.PlayerId
                                   select p;

            return playerswithcoach.ToList();
        }

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team.
        /// </summary>
        /// <returns>The lowest bankroll team.</returns>
        public IList<Player> GetPlayersLowestBankRollTeam()
        {
            List<Team> teams = this.teamRepo.GetAll().ToList();
            List<Player> players = this.playerRepo.GetAll().ToList();

            var playersresult2 = from p in players
                                 where p.TeamId == (from t in teams
                                                    join p2 in players on t.TeamId equals p2.TeamId
                                                    orderby t.BankRoll ascending
                                                    select t).First().TeamId
                                 select p;

            return playersresult2.ToList();
        }

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team async.
        /// </summary>
        /// <returns>The lowest bankroll team.</returns>
        public Task<IList<Player>> GetPlayersLowestBankRollTeamAsync()
        {
            return Task.Run(() => this.GetPlayersLowestBankRollTeam());
        }

        /// <summary>
        /// Calls the repo layer to update player.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="surname">Surname.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="jerseynumber">Jersey Number.</param>
        /// <param name="dateofbirth">Date of birth.</param>
        /// <param name="performance">Performance.</param>
        /// <returns>Boolean that tells the update was successful.</returns>
        public bool UpdatePlayer(int id, string surname, string firstname, int jerseynumber, string dateofbirth, int performance)
        {
          return this.playerRepo.UpdatePlayer(id, surname, firstname, jerseynumber, dateofbirth, performance);
        }
    }
}
