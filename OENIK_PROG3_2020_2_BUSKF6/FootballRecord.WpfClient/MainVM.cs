﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainViewModel class.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private PlayerVM selectedPlayer;
        private ObservableCollection<PlayerVM> allPlayers;

        /// <summary>
        /// Gets or sets all players.
        /// </summary>
        public ObservableCollection<PlayerVM> AllPlayers
        {
            get { return allPlayers; }
            set { Set(ref allPlayers, value); }
        }

        /// <summary>
        /// Gets or sets selected player.
        /// </summary>
        public PlayerVM SelectedPlayer
        {
            get { return selectedPlayer; }
            set { Set(ref selectedPlayer, value); }
        }

        /// <summary>
        /// Gets or sets editorFunc.
        /// </summary>
        public Func<PlayerVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            logic = new MainLogic();

            LoadCmd = new RelayCommand(() =>
                    AllPlayers = new ObservableCollection<PlayerVM>(logic.ApiGetPlayers()));
            DelCmd = new RelayCommand(() => logic.ApiDelPlayer(selectedPlayer));
            AddCmd = new RelayCommand(() => logic.EditPlayer(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditPlayer(selectedPlayer, EditorFunc));
        }
    }
}
