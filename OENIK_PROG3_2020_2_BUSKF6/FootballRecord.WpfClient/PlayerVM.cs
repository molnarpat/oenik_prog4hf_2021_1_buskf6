﻿// <copyright file="PlayerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Player ViewModel.
    /// </summary>
    public class PlayerVM : ObservableObject
    {
        private int id;
        private string surname;
        private string firstname;
        private int jerseyNumber;
        private string dateOfBirth;
        private int performance;

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        /// <summary>
        /// Gets or sets surname of player.
        /// </summary>
        public string Surname
        {
            get { return surname; }
            set { Set(ref surname, value); }
        }

        /// <summary>
        /// Gets or sets firstname of player.
        /// </summary>
        public string Firstname
        {
            get { return firstname; }
            set { Set(ref firstname, value); }
        }

        /// <summary>
        /// Gets or sets jerseyNumber of player.
        /// </summary>
        public int JerseyNumber
        {
            get { return jerseyNumber; }
            set { Set(ref jerseyNumber, value); }
        }

        /// <summary>
        /// Gets or sets dateOfBirth of player.
        /// </summary>
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { Set(ref dateOfBirth, value); }
        }

        /// <summary>
        /// Gets or sets performance of player.
        /// </summary>
        public int Performance
        {
            get { return performance; }
            set { Set(ref performance, value); }
        }

        /// <summary>
        /// Cloning method.
        /// </summary>
        /// <param name="other">Other entity.</param>
        public void CopyFrom(PlayerVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Id = other.Id;
            this.Surname = other.Surname;
            this.Firstname = other.Firstname;
            this.JerseyNumber = other.JerseyNumber;
            this.DateOfBirth = other.DateOfBirth;
            this.Performance = other.Performance;
        }
    }
}
