﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic class.
    /// </summary>
    internal class MainLogic
    {
        private string url = "http://localhost:49269/PlayersApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// SendMessage method.
        /// </summary>
        /// <param name="success">Boolean that tells succes or failed the operation.</param>
        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "PlayerResult");
        }

        /// <summary>
        /// Get players method.
        /// </summary>
        /// <returns>Players list.</returns>
        public List<PlayerVM> ApiGetPlayers()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PlayerVM>>(json, jsonOptions);
            return list;
        }

        /// <summary>
        /// Delete player method.
        /// </summary>
        /// <param name="player">Player.</param>
        public void ApiDelPlayer(PlayerVM player)
        {
            bool success = false;
            if (player != null)
            {
                string json = client.GetStringAsync(url + "del/" + player.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Edit player method.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <param name="isEditing">Is editing.</param>
        /// <returns>Boolean that tells the edit was true or false.</returns>
        private bool ApiEditPlayer(PlayerVM player, bool isEditing)
        {
            if (player == null)
            {
                return false;
            }

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", player.Id.ToString());
            }

            postData.Add("surname", player.Surname);
            postData.Add("firstname", player.Firstname);
            postData.Add("JerseyNumber", player.JerseyNumber.ToString());
            postData.Add("DateOfBirth", player.DateOfBirth);
            postData.Add("Performance", player.Performance.ToString());
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edit player.
        /// </summary>
        /// <param name="player">Player.</param>
        /// <param name="editor">Editor func.</param>
        public void EditPlayer(PlayerVM player, Func<PlayerVM, bool> editor)
        {
            PlayerVM clone = new PlayerVM();
            if (player != null)
            {
                clone.CopyFrom(player);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (player != null)
                {
                    success = ApiEditPlayer(clone, true);
                }
                else
                {
                    success = ApiEditPlayer(clone, false);
                }
            }

            SendMessage(success == true);
        }
    }
}
