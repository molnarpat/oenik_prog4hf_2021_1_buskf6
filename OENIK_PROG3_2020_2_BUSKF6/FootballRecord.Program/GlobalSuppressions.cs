﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.Main(System.String[])")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<Pending>")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.AddCoach(FootballRecord.Logic.CoachLogic,FootballRecord.Logic.PlayerLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.AddPlayer(FootballRecord.Logic.PlayerLogic,FootballRecord.Logic.TeamLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.AddTeam(FootballRecord.Logic.TeamLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.ChangeTeamBankRoll(FootballRecord.Logic.TeamLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.DeleteCoach(FootballRecord.Logic.CoachLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.DeletePlayer(FootballRecord.Logic.PlayerLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.DeleteTeam(FootballRecord.Logic.TeamLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintCoachById(FootballRecord.Logic.CoachLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintPlayerById(FootballRecord.Logic.PlayerLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintTeamById(FootballRecord.Logic.TeamLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.UpdatePlayerPerformance(FootballRecord.Logic.PlayerLogic)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.UpdateSalary(FootballRecord.Logic.CoachLogic)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintCoachById(FootballRecord.Logic.CoachLogic)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintPlayerById(FootballRecord.Logic.PlayerLogic)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:FootballRecord.Program.MyProgram.PrintTeamById(FootballRecord.Logic.TeamLogic)")]
