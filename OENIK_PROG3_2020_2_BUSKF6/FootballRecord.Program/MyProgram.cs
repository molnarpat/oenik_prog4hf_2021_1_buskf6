﻿// <copyright file="MyProgram.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballRecord.Program
{
    using System;
    using System.Collections.Generic;
    using ConsoleTools;
    using FootballRecord.Data;
    using FootballRecord.Data.Models;
    using FootballRecord.Logic;
    using FootballRecord.Repository;

    /// <summary>
    /// Main Program class.
    /// </summary>
    public class MyProgram
    {
        /// <summary>
        /// The program menu.
        /// </summary>
        /// <param name="args">The automatically generated argument. </param>
        private static void Main(string[] args)
        {
            FootballDBContext db = new FootballDBContext();
            PlayerRepository playerrepo = new PlayerRepository(db);
            TeamRepository teamrepo = new TeamRepository(db);
            CoachRepository coachrepo = new CoachRepository(db);
            TeamLogic teamlogic = new TeamLogic(playerrepo, teamrepo, coachrepo);
            PlayerLogic playerlogic = new PlayerLogic(playerrepo, coachrepo, teamrepo);
            CoachLogic coachlogic = new CoachLogic(coachrepo);

            var playermenu = new ConsoleMenu(args, level: 1)
               .Add("Az osszes jatekos kilistazasa", () => PrintPlayers(playerlogic.GetAllPlayers()))
               .Add("Egy jatekos keresese ID alapjan", () => PrintPlayerById(playerlogic))
               .Add("Jatekos hozzaadasa az adatbazishoz", () => AddPlayer(playerlogic, teamlogic))
               .Add("Jatekos torlese az adatbazisbol", () => DeletePlayer(playerlogic))
               .Add("Jatekos teljesítményének módosítása", () => UpdatePlayerPerformance(playerlogic))
               .Add("Azon jatekosok listaza,a akiknek van edzojuk", () => PrintPlayersWhoHasCoach(playerlogic))
               .Add("A lecsalacsonyabb koltsegvetesu csapat jatekosainak listazasa", () => PrintPlayersLowestBudgetTeam(playerlogic))
               .Add("[Async]A lecsalacsonyabb koltsegvetesu csapat jatekosainak listazasa", () => PrintPlayersLowestBudgetTeamAsync(playerlogic))
               .Add("Visszalepes a fomenube", ConsoleMenu.Close)
            .Configure(config =>
            {
                config.Selector = "--> ";
                config.Title = "Jatekos menu";
                config.EnableWriteTitle = true;
            });
            var teammenu = new ConsoleMenu(args, level: 1)
                .Add("Az osszes csapat kilistazasa", () => PrintTeams(teamlogic.GetAllTeams()))
                .Add("Egy csapat keresese ID alapjan", () => PrintTeamById(teamlogic))
                .Add("Csapat hozzaadasa az adatbazishoz", () => AddTeam(teamlogic))
                .Add("Csapat torlese az adatbazisbol", () => DeleteTeam(teamlogic))
                .Add("Csapat koltsegvetesenek megvaltoztatasa", () => ChangeTeamBankRoll(teamlogic))
                .Add("Atlagos teljesitmenye a jatekosoknak csapatonkent", () => TeamPerformingAverages(teamlogic))
                .Add("A csapatok atlageletkor alapjan novekvo sorrendbe rendezve", () => GetYoungestTeamsAscending(teamlogic))
                .Add("[Async]Atlagos teljesitmenye a jatekosoknak csapatonkent", () => TeamPerformingAveragesAsync(teamlogic))
                .Add("[Async]A csapatok atlageletkor alapjan novekvo sorrendbe rendezve", () => GetYoungestTeamsAscendingAsync(teamlogic))
                .Add("Visszalepes a fomenube", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Csapat Menu";
                    config.EnableWriteTitle = true;
                });
            var coachmenu = new ConsoleMenu(args, level: 1)
                  .Add("Az osszes edzo kilistazasa", () => PrintCoaches(coachlogic.GetAllCoaches()))
                  .Add("Egy edzo keresese ID alapjan", () => PrintCoachById(coachlogic))
                  .Add("Edzo hozzaadasa az adatbazishoz", () => AddCoach(coachlogic, playerlogic))
                  .Add("Edzo torlese az adatbazisbol", () => DeleteCoach(coachlogic))
                  .Add("Edzo fizetesenek megvaltoztatasa", () => UpdateSalary(coachlogic))
                 .Add("Visszalepes a fomenube", ConsoleMenu.Close)
                 .Configure(config =>
                 {
                     config.Selector = "--> ";
                     config.Title = "Coach menu";
                     config.EnableWriteTitle = true;
                 });

            var menu = new ConsoleMenu(args, level: 0)
                .Add("Jatekos muveletek", playermenu.Show)
                .Add("Csapat muveletek", teammenu.Show)
                .Add("Edzo muveletek", coachmenu.Show)
                .Add("Kilepes a rendszerbol", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Fomenu";
                    config.EnableWriteTitle = true;
                });
            menu.Show();
        }

        /// <summary>
        /// Helper method that determines a date to be correct.
        /// </summary>
        /// <param name="date">String date.</param>
        /// <returns>Returns a boolean which is true if correct is false if the date format is incorrect. </returns>
        private static bool DateStringVerify(string date)
        {
            string[] elements = date.Split('.');
            int tmp;

            if (elements.Length != 3)
            {
                return false;
            }

            if (elements[0].Length != 4)
            {
                return false;
            }

            if (!int.TryParse(elements[0], out tmp))
            {
                return false;
            }

            if (elements[1].Length != 2)
            {
                return false;
            }

            if (!int.TryParse(elements[1], out tmp))
            {
                return false;
            }

            if (elements[2].Length != 2)
            {
                return false;
            }

            if (!int.TryParse(elements[2], out tmp))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Helpmer method that tells you if a number has been entered correctly.
        /// </summary>
        /// <param name="message">Correct message. </param>
        /// <param name="errmsg">Error message. </param>
        /// <returns>Returns with a message, successful or unsuccessful.</returns>
        private static int GetIntInput(string message, string errmsg)
        {
            string tmp = string.Empty;
            int num = -1;
            do
            {
                Console.Write(message);
                tmp = Console.ReadLine();
                if (int.TryParse(tmp, out num))
                {
                    break;
                }

                Console.WriteLine(errmsg);
            }
            while (true);

            return num;
        }

        /// <summary>
        /// List players on console by Id.
        /// </summary>
        /// <param name="logic">Logic. </param>
        private static void PrintPlayerById(PlayerLogic logic)
        {
            Player p;
            do
            {
                Console.WriteLine("Add meg a keresendo jatekos ID-jat [Szam]: ");
                int id = int.Parse(Console.ReadLine());
                p = logic.GetPlayerById(id);
            }
            while (p == null);

            Console.Write(p);
            Console.ReadLine();
        }

        /// <summary>
        /// Lists all players on the console.
        /// </summary>
        /// <param name="p">List of the players.</param>
        private static void PrintPlayers(IList<Player> p)
        {
            foreach (Player item in p)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to add a player.
        /// </summary>
        /// <param name="playerlogic">Player Logic. </param>
        /// <param name="teamlogic">Team Logic. </param>
        private static void AddPlayer(PlayerLogic playerlogic, TeamLogic teamlogic)
        {
            string tmp = string.Empty;
            bool running = true;

            Console.Write("Jatekos Vezetekneve:");
            string firstname = Console.ReadLine();

            Console.Write("Jatekos Keresztneve:");
            string surname = Console.ReadLine();

            int jersey = GetIntInput("Jatekos Mezszama:", "Nem szam!");

            do
            {
                Console.Write("Jatekos Szuletesei eve [MINTA: 1234.12.01]:");
                tmp = Console.ReadLine();
            }
            while (!DateStringVerify(tmp));
            string dateOfBirth = tmp;

            int performance = GetIntInput("Jatekos Teljesitmenye:", "Nem Szam!");

            int teamId = -1;
            running = true;
            do
            {
                Console.Write("Jatekos Csapata [Az alabbiak kozul]:");
                var result = teamlogic.GetAllTeams();
                foreach (var team in result)
                {
                    Console.Write("[" + team.TeamId + "=>" + team.Name + "]");
                }

                teamId = GetIntInput("Csapat azonositoja:", "Nem valid szam!");
                foreach (var team in result)
                {
                    if (team.TeamId == teamId)
                    {
                        running = false;
                        break;
                    }
                }
            }
            while (running);

            if (playerlogic.AddPlayer(surname, firstname, jersey, dateOfBirth, performance, teamId))
            {
                Console.WriteLine("Sikeres hozzaadas! :)");
            }
            else
            {
                Console.WriteLine("Sikertelen hozzaadas! :(");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to delete a player.
        /// </summary>
        /// <param name="playerlogic">Player logic.</param>
        private static void DeletePlayer(PlayerLogic playerlogic)
        {
            int playerid = GetIntInput("Irja be a torolni kivant jatekos azonositojat", "Nem szam!");
            if (playerlogic.DeletePlayer(playerid))
            {
                Console.WriteLine("Sikeres torles!");
            }
            else
            {
                Console.WriteLine("Torles sikertelen!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to change a player performance.
        /// </summary>
        /// <param name="playerlogic">Player logic. </param>
        private static void UpdatePlayerPerformance(PlayerLogic playerlogic)
        {
            int playerid = GetIntInput("Modositani kivant jatekos azonositoja", "rossz sam!");
            int newPerf = GetIntInput("Uj teljesitmeny szam: ", "rossz szam!");

            if (playerlogic.ChangePlayerPerformance(playerid, newPerf))
            {
                Console.WriteLine("Sikeres Modositas!");
            }
            else
            {
                Console.WriteLine("Sikertelen Modositas!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to get all the players who have a coach.
        /// </summary>
        /// <param name="playerlogic">Player logic. </param>
        private static void PrintPlayersWhoHasCoach(PlayerLogic playerlogic)
        {
            foreach (var player in playerlogic.GetPlayersWithCoach())
            {
                Console.WriteLine(player);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team.
        /// </summary>
        /// <param name="playerlogic">Player logic. </param>
        private static void PrintPlayersLowestBudgetTeam(PlayerLogic playerlogic)
        {
            var result = playerlogic.GetPlayersLowestBankRollTeam();
            Console.WriteLine("A legszegenyebb csapat: " + result[0].Team.Name);
            foreach (var player in result)
            {
                Console.WriteLine(player);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the repo layer to get the lowest bankrolll team async.
        /// </summary>
        /// <param name="playerlogic">Player logic. </param>
        private static void PrintPlayersLowestBudgetTeamAsync(PlayerLogic playerlogic)
        {
            var taskResult = playerlogic.GetPlayersLowestBankRollTeamAsync();
            taskResult.Wait();
            var result = taskResult.Result;

            Console.WriteLine("A legszegenyebb csapat: " + result[0].Team.Name);
            foreach (var player in result)
            {
                Console.WriteLine(player);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to get all the team records.
        /// </summary>
        /// <param name="t">List of the teams. </param>
        private static void PrintTeams(IList<Team> t)
        {
            foreach (Team item in t)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to get a team record.
        /// </summary>
        /// <param name="logic">Team Logic.</param>
        private static void PrintTeamById(TeamLogic logic)
        {
            Team t;
            do
            {
                Console.WriteLine("Add meg a keresendo csapat ID-jat [Szam]: ");
                int id = int.Parse(Console.ReadLine());
                t = logic.GetTeamById(id);
            }
            while (t == null);

            Console.WriteLine(t);
            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to add a team.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void AddTeam(TeamLogic teamlogic)
        {
            string tmp = string.Empty;
            bool running = true;
            int szam;

            Console.Write("Csapat Neve:");
            string name = Console.ReadLine();

            do
            {
                Console.Write("Csapat Alapitasi eve [MINTA: 1234]:");
                tmp = Console.ReadLine();
                if (tmp.Length != 4)
                {
                    Console.WriteLine("Nem elegendo, vagy tul sok szamjegyet irtal be!");
                }
                else
                {
                    if (!int.TryParse(tmp, out szam))
                    {
                        running = true;
                        Console.WriteLine("Nem szamot adtal meg!");
                    }
                    else
                    {
                        running = false;
                    }
                }
            }
            while (running);
            string founded = tmp;

            Console.Write("Csapat Stadionja:");
            string stadium = Console.ReadLine();

            int bankroll = GetIntInput("Csapat koltsegvetese: ", "Nem szam!");

            if (teamlogic.AddTeam(name, founded, stadium, bankroll))
            {
                Console.WriteLine("Sikeres hozzaadas! :)");
            }
            else
            {
                Console.WriteLine("Sikertelen hozzaadas! :(");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to delete a team.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void DeleteTeam(TeamLogic teamlogic)
        {
            int teamid = GetIntInput("Irja be a torolni kivant csapat azonositojat", "Nem szam!");
            if (teamlogic.DeleteTeam(teamid))
            {
                Console.WriteLine("Sikeres torles!");
            }
            else
            {
                Console.WriteLine("Torles sikertelen!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to change a team bankroll.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void ChangeTeamBankRoll(TeamLogic teamlogic)
        {
            int teamid = GetIntInput("Modositani kivant csapat azonositoja", "Rossz szam!");
            int newbankroll = GetIntInput("Uj koltsegvetes szama: ", "Rossz szam!");
            if (teamlogic.ChangeTeamBankRoll(teamid, newbankroll))
            {
                Console.WriteLine("Sikeres Modositas!");
            }
            else
            {
                Console.WriteLine("Sikertelen Modositas!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to get a coach record.
        /// </summary>
        /// <param name="coachlogic">Coach logic. </param>
        private static void PrintCoachById(CoachLogic coachlogic)
        {
            Coach c;
            do
            {
                Console.WriteLine("Add meg a keresendo edzo ID-jat [Szam]: ");
                int id = int.Parse(Console.ReadLine());
                c = coachlogic.GetCoachById(id);
            }
            while (c == null);

            Console.Write(c);
            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to get all the coach records.
        /// </summary>
        /// <param name="c">List of the coaches.</param>
        private static void PrintCoaches(IList<Coach> c)
        {
            foreach (Coach item in c)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to add a coach.
        /// </summary>
        /// <param name="coachlogic">Coach Logic. </param>
        /// <param name="playerlogic">Player logic. </param>
        private static void AddCoach(CoachLogic coachlogic, PlayerLogic playerlogic)
        {
            Console.Write("Edzo Neve:");
            string name = Console.ReadLine();
            int salary = GetIntInput("Edzo fizetese: ", "Nem szam!");

            Console.Write("Edzo nemzetisege:");
            string nationality = Console.ReadLine();

            int addperformance = GetIntInput("Mennyivel noveli az edzo a jatekos teljesitmenyet: [szam]", "Nem szamot adtal meg!");

            int age = GetIntInput("Edzo eletkora: ", "Nem szamot adtal meg!");
            int playerid = -1;
            bool running = true;
            do
            {
                Console.Write("Melyik jatekoshoz szeretned rendelni az edzot, az alabbiak kozul? ");
                var result = playerlogic.GetAllPlayers();
                foreach (var player in result)
                {
                    Console.Write("[" + player.PlayerId + "=>" + player.Surname + ":" + player.Firstname + "]");
                }

                playerid = GetIntInput("Csapat azonositoja:", "Nem valid szam!");
                foreach (var player in result)
                {
                    if (player.PlayerId == playerid)
                    {
                        running = false;
                        break;
                    }
                }
            }
            while (running);

            if (coachlogic.AddCoach(name, salary, nationality, addperformance, age, playerid))
            {
                Console.WriteLine("Sikeres hozzaadas! :)");
            }
            else
            {
                Console.WriteLine("Sikertelen hozzaadas! :(");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to delete a coach.
        /// </summary>
        /// <param name="coachlogic">Coach Logic. </param>
        private static void DeleteCoach(CoachLogic coachlogic)
        {
            int coachid = GetIntInput("Irja be a torolni kivant edzo azonositojat", "Nem szam!");
            if (coachlogic.DeleteCoach(coachid))
            {
                Console.WriteLine("Sikeres torles!");
            }
            else
            {
                Console.WriteLine("Torles sikertelen!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic to change a coach salary.
        /// </summary>
        /// <param name="coachLogic">Coach Logic. </param>
        private static void UpdateSalary(CoachLogic coachLogic)
        {
            int coachid = GetIntInput("Modositani kivant edzo azonositoja", "Rossz szam!");
            int newsalary = GetIntInput("Uj fizetese: ", "Rossz szam!");
            if (coachLogic.ChangeCoachSalary(coachid, newsalary))
            {
                Console.WriteLine("Sikeres Modositas!");
            }
            else
            {
                Console.WriteLine("Sikertelen Modositas!");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic layer to get all the average performance.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void TeamPerformingAverages(TeamLogic teamlogic)
        {
            foreach (var item in teamlogic.GetAveragePerformance())
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the repo layer to get all the average performance async.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void TeamPerformingAveragesAsync(TeamLogic teamlogic)
        {
            var taskResult = teamlogic.GetAveragePerformanceAsync();
            taskResult.Wait();
            var result = taskResult.Result;

            foreach (var item in teamlogic.GetAveragePerformance())
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic layer to get the youngest team.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void GetYoungestTeamsAscending(TeamLogic teamlogic)
        {
            foreach (var team in teamlogic.GetYoungestTeam())
            {
                Console.WriteLine(team);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Calls the logic layer to get the youngest team async.
        /// </summary>
        /// <param name="teamlogic">Team Logic. </param>
        private static void GetYoungestTeamsAscendingAsync(TeamLogic teamlogic)
        {
            var taskResult = teamlogic.GetYoungestTeamAsync();
            taskResult.Wait();
            var result = taskResult.Result;

            foreach (var team in teamlogic.GetYoungestTeam())
            {
                Console.WriteLine(team);
            }

            Console.ReadLine();

            Console.ReadLine();
        }
    }
}
