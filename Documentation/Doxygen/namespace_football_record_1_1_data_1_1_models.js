var namespace_football_record_1_1_data_1_1_models =
[
    [ "Coach", "class_football_record_1_1_data_1_1_models_1_1_coach.html", "class_football_record_1_1_data_1_1_models_1_1_coach" ],
    [ "FootballDBContext", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context" ],
    [ "Player", "class_football_record_1_1_data_1_1_models_1_1_player.html", "class_football_record_1_1_data_1_1_models_1_1_player" ],
    [ "Team", "class_football_record_1_1_data_1_1_models_1_1_team.html", "class_football_record_1_1_data_1_1_models_1_1_team" ]
];