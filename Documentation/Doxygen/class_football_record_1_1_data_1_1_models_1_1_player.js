var class_football_record_1_1_data_1_1_models_1_1_player =
[
    [ "Player", "class_football_record_1_1_data_1_1_models_1_1_player.html#ab2f334c52f106259a33b0e5e6bafda18", null ],
    [ "Equals", "class_football_record_1_1_data_1_1_models_1_1_player.html#a232e34b5792da898859765827560b7e1", null ],
    [ "GetHashCode", "class_football_record_1_1_data_1_1_models_1_1_player.html#a3e560b6ee47d1694b2320a217b9e84ff", null ],
    [ "ToString", "class_football_record_1_1_data_1_1_models_1_1_player.html#ad404930de46378f99760c233f00c3813", null ],
    [ "Coaches", "class_football_record_1_1_data_1_1_models_1_1_player.html#a2c3bd9d1b0b7416bbf622e9e172715ff", null ],
    [ "DateOfBirth", "class_football_record_1_1_data_1_1_models_1_1_player.html#ac6c020998ca9713793416afee76c7138", null ],
    [ "Firstname", "class_football_record_1_1_data_1_1_models_1_1_player.html#a25f79f836b603bdfa3091a3de7f580bd", null ],
    [ "JerseyNumber", "class_football_record_1_1_data_1_1_models_1_1_player.html#a6b6dda04047467cfcc6be952adff140f", null ],
    [ "Performance", "class_football_record_1_1_data_1_1_models_1_1_player.html#a3946831ebf56aa5f4b9f11aa903d9a40", null ],
    [ "PlayerId", "class_football_record_1_1_data_1_1_models_1_1_player.html#afc72d05a76368efe5bfdaefd55a16ad6", null ],
    [ "Surname", "class_football_record_1_1_data_1_1_models_1_1_player.html#a1b5711e6540ad3eb6f397ca63c6b2281", null ],
    [ "Team", "class_football_record_1_1_data_1_1_models_1_1_player.html#a0b8edca2443cc65f60cdf23e8ca9359a", null ],
    [ "TeamId", "class_football_record_1_1_data_1_1_models_1_1_player.html#a2a0e1cb9d629112aeba2e86cffbb0526", null ]
];