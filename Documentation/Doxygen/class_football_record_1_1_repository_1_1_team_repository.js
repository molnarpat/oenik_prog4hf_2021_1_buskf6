var class_football_record_1_1_repository_1_1_team_repository =
[
    [ "TeamRepository", "class_football_record_1_1_repository_1_1_team_repository.html#a913874a9d8695b551b8cf4f50b9ac8e7", null ],
    [ "CreateTeam", "class_football_record_1_1_repository_1_1_team_repository.html#a465ab8c33b925bb7e7cd8de57b22788a", null ],
    [ "DeleteTeam", "class_football_record_1_1_repository_1_1_team_repository.html#a6d433fbb5916a0c7a64988d2634843bf", null ],
    [ "GetAll", "class_football_record_1_1_repository_1_1_team_repository.html#a33da55cc955390dc9af51b982cd1daae", null ],
    [ "GetById", "class_football_record_1_1_repository_1_1_team_repository.html#a9aebf7901f5f8154d2f0b83610d6fc6b", null ],
    [ "UpdateBankRoll", "class_football_record_1_1_repository_1_1_team_repository.html#af208708f91deb5bb3ab183f96bdcaddf", null ],
    [ "UpdateFounded", "class_football_record_1_1_repository_1_1_team_repository.html#a16570e27a9e8e812b42583486bcdf94e", null ],
    [ "UpdateName", "class_football_record_1_1_repository_1_1_team_repository.html#ac0f4a2255b0f276a9da269b8f79f0ee8", null ],
    [ "UpdateStadium", "class_football_record_1_1_repository_1_1_team_repository.html#a35e545e36a06141ff58a9db6986558b8", null ]
];