var interface_football_record_1_1_repository_1_1_i_team_repository =
[
    [ "CreateTeam", "interface_football_record_1_1_repository_1_1_i_team_repository.html#a84c900d193ce20de10a6c2d9870d657b", null ],
    [ "DeleteTeam", "interface_football_record_1_1_repository_1_1_i_team_repository.html#a9e0a0dd2c8dc7ff9b6ab2af113014318", null ],
    [ "UpdateBankRoll", "interface_football_record_1_1_repository_1_1_i_team_repository.html#ab4fb0272b45399af084acc3b59539f58", null ],
    [ "UpdateFounded", "interface_football_record_1_1_repository_1_1_i_team_repository.html#a302b263a6fc336191a5cd10ed74a15df", null ],
    [ "UpdateName", "interface_football_record_1_1_repository_1_1_i_team_repository.html#a27122d15cc4b292a1f15e4d72bc8932a", null ],
    [ "UpdateStadium", "interface_football_record_1_1_repository_1_1_i_team_repository.html#a5ceb2457bcd3a12cd6c7598536bbdf56", null ]
];