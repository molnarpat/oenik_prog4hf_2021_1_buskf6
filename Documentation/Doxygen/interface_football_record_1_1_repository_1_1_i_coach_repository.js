var interface_football_record_1_1_repository_1_1_i_coach_repository =
[
    [ "CreateCoach", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#ae5416f444f9a609a0d579d3b85fca1aa", null ],
    [ "DeleteCoach", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#a2e7f458ffcacafeff5d40a3341a29277", null ],
    [ "UpdateAddPerformance", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#a0633a93c3a07dab34f75a82e91c964b3", null ],
    [ "UpdateAge", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#a16ecf67aca048ecd43708ea4db81a7b9", null ],
    [ "UpdateName", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#a47ba3b95a355031b200b76c3f40a407d", null ],
    [ "UpdateNationality", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#ac5a1ea3826c899bddb21755cfa37ea58", null ],
    [ "UpdateSalary", "interface_football_record_1_1_repository_1_1_i_coach_repository.html#af220e6f98b17009e35144aaf50382c71", null ]
];