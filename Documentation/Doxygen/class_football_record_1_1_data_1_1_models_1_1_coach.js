var class_football_record_1_1_data_1_1_models_1_1_coach =
[
    [ "Equals", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a4d1b25783eb42ef014fd69be382a9b16", null ],
    [ "GetHashCode", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a4a55013ff5597cb426fd87169eb62a11", null ],
    [ "ToString", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a3ef463e4b2e67b802e1db7848d549e13", null ],
    [ "AddPerformance", "class_football_record_1_1_data_1_1_models_1_1_coach.html#aad38aa00e9af78075861435e93f6d375", null ],
    [ "Age", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a2c8b8081742c9165ad5917c50fcbd22d", null ],
    [ "CoachId", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a6d5966ac54cae5dec9ecca1d6597fd27", null ],
    [ "Name", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a75ee498ead7c979d4cd78bd85d2a16f6", null ],
    [ "Nationality", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a363839d7aea8394e8f1e383557abdf94", null ],
    [ "Player", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a273725a167af28d488df507838d8a78c", null ],
    [ "PlayerId", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a33053bb9c86e6ce08a71fe10b3bb18ad", null ],
    [ "Salary", "class_football_record_1_1_data_1_1_models_1_1_coach.html#a1a7c05f9cfec88d4e1a49e33a8f2f18a", null ]
];