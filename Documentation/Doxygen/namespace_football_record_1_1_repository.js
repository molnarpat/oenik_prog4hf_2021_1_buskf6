var namespace_football_record_1_1_repository =
[
    [ "CoachRepository", "class_football_record_1_1_repository_1_1_coach_repository.html", "class_football_record_1_1_repository_1_1_coach_repository" ],
    [ "ICoachRepository", "interface_football_record_1_1_repository_1_1_i_coach_repository.html", "interface_football_record_1_1_repository_1_1_i_coach_repository" ],
    [ "IPlayerRepository", "interface_football_record_1_1_repository_1_1_i_player_repository.html", "interface_football_record_1_1_repository_1_1_i_player_repository" ],
    [ "IRepository", "interface_football_record_1_1_repository_1_1_i_repository.html", "interface_football_record_1_1_repository_1_1_i_repository" ],
    [ "ITeamRepository", "interface_football_record_1_1_repository_1_1_i_team_repository.html", "interface_football_record_1_1_repository_1_1_i_team_repository" ],
    [ "PlayerRepository", "class_football_record_1_1_repository_1_1_player_repository.html", "class_football_record_1_1_repository_1_1_player_repository" ],
    [ "TeamRepository", "class_football_record_1_1_repository_1_1_team_repository.html", "class_football_record_1_1_repository_1_1_team_repository" ]
];