var class_football_record_1_1_logic_1_1_player_logic =
[
    [ "PlayerLogic", "class_football_record_1_1_logic_1_1_player_logic.html#a290c5a863a1e39ba22c7154c3b00e3c4", null ],
    [ "AddPlayer", "class_football_record_1_1_logic_1_1_player_logic.html#a1e096df2ee3136e32a86cb32ab01257f", null ],
    [ "ChangePlayerPerformance", "class_football_record_1_1_logic_1_1_player_logic.html#ab2cd73487b4b6f11eef2207a68d1c64a", null ],
    [ "DeletePlayer", "class_football_record_1_1_logic_1_1_player_logic.html#a93b74c1edbedcbb3497ce8db4a88c3f5", null ],
    [ "GetAllPlayers", "class_football_record_1_1_logic_1_1_player_logic.html#acf0d1ba1b3790a5272827fca9c586df0", null ],
    [ "GetCurrentPlayerIDs", "class_football_record_1_1_logic_1_1_player_logic.html#aa0dad389a0b350ef51628dd52cb5b6ad", null ],
    [ "GetPlayerById", "class_football_record_1_1_logic_1_1_player_logic.html#a442f17b2a04ecfbba9f537137547c664", null ],
    [ "GetPlayersLowestBankRollTeam", "class_football_record_1_1_logic_1_1_player_logic.html#a78682447be55242ad7a8027a8f0fb1cb", null ],
    [ "GetPlayersLowestBankRollTeamAsync", "class_football_record_1_1_logic_1_1_player_logic.html#abd11a20e924d3494fff9680f4a265f61", null ],
    [ "GetPlayersWithCoach", "class_football_record_1_1_logic_1_1_player_logic.html#a5d8e1dca252101514ebcce573aeeb528", null ]
];