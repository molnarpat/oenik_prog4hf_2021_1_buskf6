var hierarchy =
[
    [ "FootballRecord.Logic.AveragesResult", "class_football_record_1_1_logic_1_1_averages_result.html", null ],
    [ "FootballRecord.Data.Models.Coach", "class_football_record_1_1_data_1_1_models_1_1_coach.html", null ],
    [ "DbContext", null, [
      [ "FootballRecord.Data.Models.FootballDBContext", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html", null ]
    ] ],
    [ "FootballRecord.Logic.ICoachLogic", "interface_football_record_1_1_logic_1_1_i_coach_logic.html", [
      [ "FootballRecord.Logic.CoachLogic", "class_football_record_1_1_logic_1_1_coach_logic.html", null ]
    ] ],
    [ "FootballRecord.Logic.IPlayerLogic", "interface_football_record_1_1_logic_1_1_i_player_logic.html", [
      [ "FootballRecord.Logic.PlayerLogic", "class_football_record_1_1_logic_1_1_player_logic.html", null ]
    ] ],
    [ "FootballRecord.Repository.IRepository< T >", "interface_football_record_1_1_repository_1_1_i_repository.html", null ],
    [ "FootballRecord.Repository.IRepository< Coach >", "interface_football_record_1_1_repository_1_1_i_repository.html", [
      [ "FootballRecord.Repository.ICoachRepository", "interface_football_record_1_1_repository_1_1_i_coach_repository.html", [
        [ "FootballRecord.Repository.CoachRepository", "class_football_record_1_1_repository_1_1_coach_repository.html", null ]
      ] ]
    ] ],
    [ "FootballRecord.Repository.IRepository< Player >", "interface_football_record_1_1_repository_1_1_i_repository.html", [
      [ "FootballRecord.Repository.IPlayerRepository", "interface_football_record_1_1_repository_1_1_i_player_repository.html", [
        [ "FootballRecord.Repository.PlayerRepository", "class_football_record_1_1_repository_1_1_player_repository.html", null ]
      ] ]
    ] ],
    [ "FootballRecord.Repository.IRepository< Team >", "interface_football_record_1_1_repository_1_1_i_repository.html", [
      [ "FootballRecord.Repository.ITeamRepository", "interface_football_record_1_1_repository_1_1_i_team_repository.html", [
        [ "FootballRecord.Repository.TeamRepository", "class_football_record_1_1_repository_1_1_team_repository.html", null ]
      ] ]
    ] ],
    [ "FootballRecord.Logic.ITeamLogic", "interface_football_record_1_1_logic_1_1_i_team_logic.html", [
      [ "FootballRecord.Logic.TeamLogic", "class_football_record_1_1_logic_1_1_team_logic.html", null ]
    ] ],
    [ "FootballRecord.Logic.Tests.LogicTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "FootballRecord.Program.MyProgram", "class_football_record_1_1_program_1_1_my_program.html", null ],
    [ "FootballRecord.Data.Models.Player", "class_football_record_1_1_data_1_1_models_1_1_player.html", null ],
    [ "FootballRecord.Data.Models.Team", "class_football_record_1_1_data_1_1_models_1_1_team.html", null ]
];