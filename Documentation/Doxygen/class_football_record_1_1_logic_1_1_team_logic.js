var class_football_record_1_1_logic_1_1_team_logic =
[
    [ "TeamLogic", "class_football_record_1_1_logic_1_1_team_logic.html#abb60dbbe0d68ab8146e290cae0a23e71", null ],
    [ "AddTeam", "class_football_record_1_1_logic_1_1_team_logic.html#af8d34bd129f2bdbfcad7ddca156a99cf", null ],
    [ "ChangeTeamBankRoll", "class_football_record_1_1_logic_1_1_team_logic.html#acf65e50e092eb711ba773318cba160ca", null ],
    [ "DeleteTeam", "class_football_record_1_1_logic_1_1_team_logic.html#afce9ce2c011e97969afcebfc3fdf460e", null ],
    [ "GetAllTeams", "class_football_record_1_1_logic_1_1_team_logic.html#ac1aa0b7086addcfab1b459080c309e4e", null ],
    [ "GetAveragePerformance", "class_football_record_1_1_logic_1_1_team_logic.html#a6ce3412edc6747248deadc82644eb598", null ],
    [ "GetAveragePerformanceAsync", "class_football_record_1_1_logic_1_1_team_logic.html#a3610f8adccae6f39c4748bc080e7553c", null ],
    [ "GetCurrentTeamIDs", "class_football_record_1_1_logic_1_1_team_logic.html#a446c333d77c31a1f7648f579963f4ff7", null ],
    [ "GetTeamById", "class_football_record_1_1_logic_1_1_team_logic.html#a819f82f4cd59861571d034f95d877708", null ],
    [ "GetYoungestTeam", "class_football_record_1_1_logic_1_1_team_logic.html#a55e95745c0f17c5053344e5e7f181e04", null ],
    [ "GetYoungestTeamAsync", "class_football_record_1_1_logic_1_1_team_logic.html#a6681309e19a0bd5af50ae6efc3da462c", null ]
];