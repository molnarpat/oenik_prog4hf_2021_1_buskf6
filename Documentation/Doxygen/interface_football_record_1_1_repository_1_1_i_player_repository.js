var interface_football_record_1_1_repository_1_1_i_player_repository =
[
    [ "CreatePlayer", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a449a48cf4b932c4b46f4dff502dd19ad", null ],
    [ "DeletePlayer", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a02fd603a5c7a3478ff3d3796bf954255", null ],
    [ "UpdateDateOfBirth", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a43fc4a9142dfd6842f69ec379e25ed74", null ],
    [ "UpdateFirstname", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a9fe73fd3c2ade4f68a98d434869b1d6c", null ],
    [ "UpdateJerseyNumber", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a48cb80c80bdefc1731c553a910130643", null ],
    [ "UpdatePerformance", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a51e9d10e56a4708c3d3822c0e39318f0", null ],
    [ "UpdateSurname", "interface_football_record_1_1_repository_1_1_i_player_repository.html#a95e752138e4a69279d92f66cef383c91", null ]
];