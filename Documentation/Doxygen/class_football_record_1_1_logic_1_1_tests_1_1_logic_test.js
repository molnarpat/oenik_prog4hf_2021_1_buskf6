var class_football_record_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "AddPlayerTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a69410750fbd04db6f40db2add6b2a69e", null ],
    [ "ChangeCoachSalaryTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#aaa45bb6eb5c6ac03d5695706495bffd3", null ],
    [ "DeletePlayerTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#aeb15fa3b2dcd800101db4e32a010aff8", null ],
    [ "DeleteTeamTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#af833e2f37cddadc3c992518687d1a649", null ],
    [ "GetAllTeamsTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#af859bd8f92e23cfdcb703dc2da062713", null ],
    [ "GetOnePlayerTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a5f2b8b7f0553de82b7b6f0edd303977a", null ],
    [ "Initialize", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a4576758e88546d8a93d21fc84158d08a", null ],
    [ "LowestBankrollPlayers", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#aa9cb4553bea85b695508a8e3231fa51d", null ],
    [ "TeamAveragePerformanceTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a894b44cf30ec7d6833dc7845a6e05a09", null ],
    [ "UpdatePlayerPerformanceTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a48375079c1ab3e5f1dea07f3ce857e5c", null ],
    [ "YoungestTeamTest", "class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#acf981976cf3f157cc1142ef396c3c16f", null ]
];