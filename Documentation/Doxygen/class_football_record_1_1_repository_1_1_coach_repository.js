var class_football_record_1_1_repository_1_1_coach_repository =
[
    [ "CoachRepository", "class_football_record_1_1_repository_1_1_coach_repository.html#ac9bf5e21df4a75d6286b00e6004e3087", null ],
    [ "CreateCoach", "class_football_record_1_1_repository_1_1_coach_repository.html#ae3b464d6dea8d6b899b12a0815090fa7", null ],
    [ "DeleteCoach", "class_football_record_1_1_repository_1_1_coach_repository.html#a938a68b98d70f9df60b7210fd25a03b7", null ],
    [ "GetAll", "class_football_record_1_1_repository_1_1_coach_repository.html#ab42496cb182115d0cad0ae1711d27b54", null ],
    [ "GetById", "class_football_record_1_1_repository_1_1_coach_repository.html#a6c970097b9eefc06ecea27a18f77b429", null ],
    [ "UpdateAddPerformance", "class_football_record_1_1_repository_1_1_coach_repository.html#a25b7d116aa0ad7aabb158183fdae1c5e", null ],
    [ "UpdateAge", "class_football_record_1_1_repository_1_1_coach_repository.html#a930c42ba3ccf9365078d04fe2607d434", null ],
    [ "UpdateName", "class_football_record_1_1_repository_1_1_coach_repository.html#a5244112bd7d0ef14462d02ff8c0cb0da", null ],
    [ "UpdateNationality", "class_football_record_1_1_repository_1_1_coach_repository.html#a02c02a5d7cb9546aa969bf70b884c2bc", null ],
    [ "UpdateSalary", "class_football_record_1_1_repository_1_1_coach_repository.html#abb88155061ef976ceffe9a1bc1da35c4", null ]
];