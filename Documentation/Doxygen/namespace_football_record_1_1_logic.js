var namespace_football_record_1_1_logic =
[
    [ "Tests", "namespace_football_record_1_1_logic_1_1_tests.html", "namespace_football_record_1_1_logic_1_1_tests" ],
    [ "AveragesResult", "class_football_record_1_1_logic_1_1_averages_result.html", "class_football_record_1_1_logic_1_1_averages_result" ],
    [ "CoachLogic", "class_football_record_1_1_logic_1_1_coach_logic.html", "class_football_record_1_1_logic_1_1_coach_logic" ],
    [ "ICoachLogic", "interface_football_record_1_1_logic_1_1_i_coach_logic.html", "interface_football_record_1_1_logic_1_1_i_coach_logic" ],
    [ "IPlayerLogic", "interface_football_record_1_1_logic_1_1_i_player_logic.html", "interface_football_record_1_1_logic_1_1_i_player_logic" ],
    [ "ITeamLogic", "interface_football_record_1_1_logic_1_1_i_team_logic.html", "interface_football_record_1_1_logic_1_1_i_team_logic" ],
    [ "PlayerLogic", "class_football_record_1_1_logic_1_1_player_logic.html", "class_football_record_1_1_logic_1_1_player_logic" ],
    [ "TeamLogic", "class_football_record_1_1_logic_1_1_team_logic.html", "class_football_record_1_1_logic_1_1_team_logic" ]
];