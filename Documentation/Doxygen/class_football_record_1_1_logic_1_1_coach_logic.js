var class_football_record_1_1_logic_1_1_coach_logic =
[
    [ "CoachLogic", "class_football_record_1_1_logic_1_1_coach_logic.html#a8dbe41ff76d581f395fe13eab0d2a9c7", null ],
    [ "AddCoach", "class_football_record_1_1_logic_1_1_coach_logic.html#a726e392337b1b67133df35fa4342e2f0", null ],
    [ "ChangeCoachSalary", "class_football_record_1_1_logic_1_1_coach_logic.html#a927215606b6557e44f3a60bd29e09b45", null ],
    [ "DeleteCoach", "class_football_record_1_1_logic_1_1_coach_logic.html#ac0b6f0af45d02afe39cd44143b54902f", null ],
    [ "GetAllCoaches", "class_football_record_1_1_logic_1_1_coach_logic.html#a47b2a130f34b0f892ebf836a9110d76d", null ],
    [ "GetCoachById", "class_football_record_1_1_logic_1_1_coach_logic.html#af3fb137febfcceadb3a0b40db1ec1874", null ],
    [ "GetCurrentCoachIDs", "class_football_record_1_1_logic_1_1_coach_logic.html#a66df4e9fbf8ccb85761a4c5774c91eb1", null ]
];