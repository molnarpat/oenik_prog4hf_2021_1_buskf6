var class_football_record_1_1_repository_1_1_player_repository =
[
    [ "PlayerRepository", "class_football_record_1_1_repository_1_1_player_repository.html#a2bebc5c8b9de14ba095a5ac7d5872b91", null ],
    [ "CreatePlayer", "class_football_record_1_1_repository_1_1_player_repository.html#abaf1f718a6521792866d339dc2c16895", null ],
    [ "DeletePlayer", "class_football_record_1_1_repository_1_1_player_repository.html#affd0b709cabdf32574561312d65b7b16", null ],
    [ "GetAll", "class_football_record_1_1_repository_1_1_player_repository.html#aab1efd678c948ce6bbd1c8320c0d9a46", null ],
    [ "GetById", "class_football_record_1_1_repository_1_1_player_repository.html#a7a7f592a3beb76bedce0e4b6aeee0b9b", null ],
    [ "UpdateDateOfBirth", "class_football_record_1_1_repository_1_1_player_repository.html#a1e92cff3f439267894e493f333775922", null ],
    [ "UpdateFirstname", "class_football_record_1_1_repository_1_1_player_repository.html#a879289ef4cbfe6f1a6d439e85c8110e2", null ],
    [ "UpdateJerseyNumber", "class_football_record_1_1_repository_1_1_player_repository.html#a96ff2d6e08021d9288914918ae3ed7df", null ],
    [ "UpdatePerformance", "class_football_record_1_1_repository_1_1_player_repository.html#a1fee596dcaedfa454e86d9b2eb00b0a1", null ],
    [ "UpdateSurname", "class_football_record_1_1_repository_1_1_player_repository.html#a7a52edd94820e314584a1fd1aa655869", null ]
];