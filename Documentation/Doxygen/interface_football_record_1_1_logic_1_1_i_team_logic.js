var interface_football_record_1_1_logic_1_1_i_team_logic =
[
    [ "AddTeam", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a90a400f80636a924ed61645533ba204e", null ],
    [ "ChangeTeamBankRoll", "interface_football_record_1_1_logic_1_1_i_team_logic.html#ab6d7faf25f790f47578965a1d1b50307", null ],
    [ "DeleteTeam", "interface_football_record_1_1_logic_1_1_i_team_logic.html#ae1d90449d6edb7d1bd9817af515c429e", null ],
    [ "GetAllTeams", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a49317614f0feac036c27ab12cdc95e4c", null ],
    [ "GetAveragePerformance", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a9fb246d64a973c1e3e68a2544aa834d7", null ],
    [ "GetAveragePerformanceAsync", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a6b5753ef997c4586a1b1af0c278c47d4", null ],
    [ "GetCurrentTeamIDs", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a6dbdd97b622e1d71febb51faf8ece9cc", null ],
    [ "GetTeamById", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a215a4db1d86f45afbc1586aba8159742", null ],
    [ "GetYoungestTeam", "interface_football_record_1_1_logic_1_1_i_team_logic.html#ab8cdd5be164d2c196496bfaf81c3aadb", null ],
    [ "GetYoungestTeamAsync", "interface_football_record_1_1_logic_1_1_i_team_logic.html#a91a9211d77303c93428aedfcf2a70d46", null ]
];