var interface_football_record_1_1_logic_1_1_i_coach_logic =
[
    [ "AddCoach", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#ad8f0bd0ffd41ec1a47550b166f433059", null ],
    [ "ChangeCoachSalary", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#acbc1f2c48dfa79e0a12ab3dcc9dd6529", null ],
    [ "DeleteCoach", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#a4d8cb18e0599fe5fc126a403cd9021d4", null ],
    [ "GetAllCoaches", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#aab26acdac928f61dd728bd5ba408ac45", null ],
    [ "GetCoachById", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#a61325d6d129a3b659ee5c0dc9c34d9c1", null ],
    [ "GetCurrentCoachIDs", "interface_football_record_1_1_logic_1_1_i_coach_logic.html#a2a42bea7f77837fb8c1bb1d9ae5abb0a", null ]
];