var interface_football_record_1_1_logic_1_1_i_player_logic =
[
    [ "AddPlayer", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a997a96ba5c431fc617f9ef22b044b59a", null ],
    [ "ChangePlayerPerformance", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a83e31996123fc113139c87ee3955a347", null ],
    [ "DeletePlayer", "interface_football_record_1_1_logic_1_1_i_player_logic.html#aa1c3c51ee598ae75de138999e3389389", null ],
    [ "GetAllPlayers", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a6a345466fc0bc58326f352543f20e6f1", null ],
    [ "GetCurrentPlayerIDs", "interface_football_record_1_1_logic_1_1_i_player_logic.html#ada1ed1c2d499e8b4bc725849f4adbcd6", null ],
    [ "GetPlayerById", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a80cdeb4fe23be214de334c0b3269d0b1", null ],
    [ "GetPlayersLowestBankRollTeam", "interface_football_record_1_1_logic_1_1_i_player_logic.html#aef16dc12cb5c7ee5f9575b7bf500002f", null ],
    [ "GetPlayersLowestBankRollTeamAsync", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a27fca1e29f6e953a89e73a19c1b8f6bf", null ],
    [ "GetPlayersWithCoach", "interface_football_record_1_1_logic_1_1_i_player_logic.html#a7f19a8aea0ffaa493e24615b19c28368", null ]
];