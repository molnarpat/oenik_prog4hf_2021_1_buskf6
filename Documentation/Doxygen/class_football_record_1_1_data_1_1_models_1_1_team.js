var class_football_record_1_1_data_1_1_models_1_1_team =
[
    [ "Team", "class_football_record_1_1_data_1_1_models_1_1_team.html#a4539c55dd879df43a62eabba42a74df9", null ],
    [ "Equals", "class_football_record_1_1_data_1_1_models_1_1_team.html#a1fe5e63df1d2a853b695d66e092d5bd0", null ],
    [ "GetHashCode", "class_football_record_1_1_data_1_1_models_1_1_team.html#a8a2fd60a19506185ef36de227cd940f6", null ],
    [ "ToString", "class_football_record_1_1_data_1_1_models_1_1_team.html#adfec9dc5fd82dd42633e21208a8a11c7", null ],
    [ "BankRoll", "class_football_record_1_1_data_1_1_models_1_1_team.html#a7bf66b85c040dcd216d3d0f8e1a1d7d2", null ],
    [ "Founded", "class_football_record_1_1_data_1_1_models_1_1_team.html#a084a32b6ea478488e99e84be80797999", null ],
    [ "Name", "class_football_record_1_1_data_1_1_models_1_1_team.html#acb2bcde94699c039055a1725ec62606b", null ],
    [ "Players", "class_football_record_1_1_data_1_1_models_1_1_team.html#ada89223afd4b77135c971a0aceca38d2", null ],
    [ "Stadium", "class_football_record_1_1_data_1_1_models_1_1_team.html#aa29f9926efb1b1659f0b5fd018bcb75d", null ],
    [ "TeamId", "class_football_record_1_1_data_1_1_models_1_1_team.html#a6b318c7dcc9af4058b2df1d958a8e120", null ]
];