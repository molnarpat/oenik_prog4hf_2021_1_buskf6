var searchData=
[
  ['performance_220',['Performance',['../class_football_record_1_1_data_1_1_models_1_1_player.html#a3946831ebf56aa5f4b9f11aa903d9a40',1,'FootballRecord::Data::Models::Player']]],
  ['player_221',['Player',['../class_football_record_1_1_data_1_1_models_1_1_coach.html#a273725a167af28d488df507838d8a78c',1,'FootballRecord::Data::Models::Coach']]],
  ['playerid_222',['PlayerId',['../class_football_record_1_1_data_1_1_models_1_1_coach.html#a33053bb9c86e6ce08a71fe10b3bb18ad',1,'FootballRecord.Data.Models.Coach.PlayerId()'],['../class_football_record_1_1_data_1_1_models_1_1_player.html#afc72d05a76368efe5bfdaefd55a16ad6',1,'FootballRecord.Data.Models.Player.PlayerId()']]],
  ['players_223',['Players',['../class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a05f7e47169b39e46a999819e1a14268e',1,'FootballRecord.Data.Models.FootballDBContext.Players()'],['../class_football_record_1_1_data_1_1_models_1_1_team.html#ada89223afd4b77135c971a0aceca38d2',1,'FootballRecord.Data.Models.Team.Players()']]]
];
