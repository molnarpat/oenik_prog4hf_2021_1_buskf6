var searchData=
[
  ['icoachlogic_59',['ICoachLogic',['../interface_football_record_1_1_logic_1_1_i_coach_logic.html',1,'FootballRecord::Logic']]],
  ['icoachrepository_60',['ICoachRepository',['../interface_football_record_1_1_repository_1_1_i_coach_repository.html',1,'FootballRecord::Repository']]],
  ['initialize_61',['Initialize',['../class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a4576758e88546d8a93d21fc84158d08a',1,'FootballRecord::Logic::Tests::LogicTest']]],
  ['iplayerlogic_62',['IPlayerLogic',['../interface_football_record_1_1_logic_1_1_i_player_logic.html',1,'FootballRecord::Logic']]],
  ['iplayerrepository_63',['IPlayerRepository',['../interface_football_record_1_1_repository_1_1_i_player_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_64',['IRepository',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20coach_20_3e_65',['IRepository&lt; Coach &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20player_20_3e_66',['IRepository&lt; Player &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20team_20_3e_67',['IRepository&lt; Team &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['iteamlogic_68',['ITeamLogic',['../interface_football_record_1_1_logic_1_1_i_team_logic.html',1,'FootballRecord::Logic']]],
  ['iteamrepository_69',['ITeamRepository',['../interface_football_record_1_1_repository_1_1_i_team_repository.html',1,'FootballRecord::Repository']]]
];
