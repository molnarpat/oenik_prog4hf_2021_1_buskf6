var searchData=
[
  ['data_28',['Data',['../namespace_football_record_1_1_data.html',1,'FootballRecord']]],
  ['firstname_29',['Firstname',['../class_football_record_1_1_data_1_1_models_1_1_player.html#a25f79f836b603bdfa3091a3de7f580bd',1,'FootballRecord::Data::Models::Player']]],
  ['footballdbcontext_30',['FootballDBContext',['../class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html',1,'FootballRecord.Data.Models.FootballDBContext'],['../class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#ae9d03f5e581537e16c21a860d327f732',1,'FootballRecord.Data.Models.FootballDBContext.FootballDBContext(DbContextOptions&lt; FootballDBContext &gt; options)'],['../class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a0a85f35a9eb9801e40e489012b8de619',1,'FootballRecord.Data.Models.FootballDBContext.FootballDBContext()']]],
  ['footballrecord_31',['FootballRecord',['../namespace_football_record.html',1,'']]],
  ['founded_32',['Founded',['../class_football_record_1_1_data_1_1_models_1_1_team.html#a084a32b6ea478488e99e84be80797999',1,'FootballRecord::Data::Models::Team']]],
  ['logic_33',['Logic',['../namespace_football_record_1_1_logic.html',1,'FootballRecord']]],
  ['models_34',['Models',['../namespace_football_record_1_1_data_1_1_models.html',1,'FootballRecord::Data']]],
  ['program_35',['Program',['../namespace_football_record_1_1_program.html',1,'FootballRecord']]],
  ['repository_36',['Repository',['../namespace_football_record_1_1_repository.html',1,'FootballRecord']]],
  ['tests_37',['Tests',['../namespace_football_record_1_1_logic_1_1_tests.html',1,'FootballRecord::Logic']]]
];
