var searchData=
[
  ['team_227',['Team',['../class_football_record_1_1_data_1_1_models_1_1_player.html#a0b8edca2443cc65f60cdf23e8ca9359a',1,'FootballRecord::Data::Models::Player']]],
  ['teamid_228',['TeamId',['../class_football_record_1_1_data_1_1_models_1_1_player.html#a2a0e1cb9d629112aeba2e86cffbb0526',1,'FootballRecord.Data.Models.Player.TeamId()'],['../class_football_record_1_1_data_1_1_models_1_1_team.html#a6b318c7dcc9af4058b2df1d958a8e120',1,'FootballRecord.Data.Models.Team.TeamId()']]],
  ['teamname_229',['TeamName',['../class_football_record_1_1_logic_1_1_averages_result.html#ac6435401a9054b1f9a38dc0869952c35',1,'FootballRecord::Logic::AveragesResult']]],
  ['teams_230',['Teams',['../class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a32c891070520a946315dcb1e66c7bfa9',1,'FootballRecord::Data::Models::FootballDBContext']]]
];
