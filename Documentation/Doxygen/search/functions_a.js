var searchData=
[
  ['team_188',['Team',['../class_football_record_1_1_data_1_1_models_1_1_team.html#a4539c55dd879df43a62eabba42a74df9',1,'FootballRecord::Data::Models::Team']]],
  ['teamaverageperformancetest_189',['TeamAveragePerformanceTest',['../class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a894b44cf30ec7d6833dc7845a6e05a09',1,'FootballRecord::Logic::Tests::LogicTest']]],
  ['teamlogic_190',['TeamLogic',['../class_football_record_1_1_logic_1_1_team_logic.html#abb60dbbe0d68ab8146e290cae0a23e71',1,'FootballRecord::Logic::TeamLogic']]],
  ['teamrepository_191',['TeamRepository',['../class_football_record_1_1_repository_1_1_team_repository.html#a913874a9d8695b551b8cf4f50b9ac8e7',1,'FootballRecord::Repository::TeamRepository']]],
  ['tostring_192',['ToString',['../class_football_record_1_1_data_1_1_models_1_1_coach.html#a3ef463e4b2e67b802e1db7848d549e13',1,'FootballRecord.Data.Models.Coach.ToString()'],['../class_football_record_1_1_data_1_1_models_1_1_player.html#ad404930de46378f99760c233f00c3813',1,'FootballRecord.Data.Models.Player.ToString()'],['../class_football_record_1_1_data_1_1_models_1_1_team.html#adfec9dc5fd82dd42633e21208a8a11c7',1,'FootballRecord.Data.Models.Team.ToString()'],['../class_football_record_1_1_logic_1_1_averages_result.html#a25d5fcd617648b4bac6410dfb25d8356',1,'FootballRecord.Logic.AveragesResult.ToString()']]]
];
