var searchData=
[
  ['addcoach_0',['AddCoach',['../class_football_record_1_1_logic_1_1_coach_logic.html#a726e392337b1b67133df35fa4342e2f0',1,'FootballRecord.Logic.CoachLogic.AddCoach()'],['../interface_football_record_1_1_logic_1_1_i_coach_logic.html#ad8f0bd0ffd41ec1a47550b166f433059',1,'FootballRecord.Logic.ICoachLogic.AddCoach()']]],
  ['addperformance_1',['AddPerformance',['../class_football_record_1_1_data_1_1_models_1_1_coach.html#aad38aa00e9af78075861435e93f6d375',1,'FootballRecord::Data::Models::Coach']]],
  ['addplayer_2',['AddPlayer',['../interface_football_record_1_1_logic_1_1_i_player_logic.html#a997a96ba5c431fc617f9ef22b044b59a',1,'FootballRecord.Logic.IPlayerLogic.AddPlayer()'],['../class_football_record_1_1_logic_1_1_player_logic.html#a1e096df2ee3136e32a86cb32ab01257f',1,'FootballRecord.Logic.PlayerLogic.AddPlayer()']]],
  ['addplayertest_3',['AddPlayerTest',['../class_football_record_1_1_logic_1_1_tests_1_1_logic_test.html#a69410750fbd04db6f40db2add6b2a69e',1,'FootballRecord::Logic::Tests::LogicTest']]],
  ['addteam_4',['AddTeam',['../interface_football_record_1_1_logic_1_1_i_team_logic.html#a90a400f80636a924ed61645533ba204e',1,'FootballRecord.Logic.ITeamLogic.AddTeam()'],['../class_football_record_1_1_logic_1_1_team_logic.html#af8d34bd129f2bdbfcad7ddca156a99cf',1,'FootballRecord.Logic.TeamLogic.AddTeam()']]],
  ['age_5',['Age',['../class_football_record_1_1_data_1_1_models_1_1_coach.html#a2c8b8081742c9165ad5917c50fcbd22d',1,'FootballRecord::Data::Models::Coach']]],
  ['averageperformance_6',['AveragePerformance',['../class_football_record_1_1_logic_1_1_averages_result.html#aef1bc939793b9fe1cf2f5813bae2dac1',1,'FootballRecord::Logic::AveragesResult']]],
  ['averagesresult_7',['AveragesResult',['../class_football_record_1_1_logic_1_1_averages_result.html',1,'FootballRecord::Logic']]]
];
