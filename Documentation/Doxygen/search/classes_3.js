var searchData=
[
  ['icoachlogic_115',['ICoachLogic',['../interface_football_record_1_1_logic_1_1_i_coach_logic.html',1,'FootballRecord::Logic']]],
  ['icoachrepository_116',['ICoachRepository',['../interface_football_record_1_1_repository_1_1_i_coach_repository.html',1,'FootballRecord::Repository']]],
  ['iplayerlogic_117',['IPlayerLogic',['../interface_football_record_1_1_logic_1_1_i_player_logic.html',1,'FootballRecord::Logic']]],
  ['iplayerrepository_118',['IPlayerRepository',['../interface_football_record_1_1_repository_1_1_i_player_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_119',['IRepository',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20coach_20_3e_120',['IRepository&lt; Coach &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20player_20_3e_121',['IRepository&lt; Player &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['irepository_3c_20team_20_3e_122',['IRepository&lt; Team &gt;',['../interface_football_record_1_1_repository_1_1_i_repository.html',1,'FootballRecord::Repository']]],
  ['iteamlogic_123',['ITeamLogic',['../interface_football_record_1_1_logic_1_1_i_team_logic.html',1,'FootballRecord::Logic']]],
  ['iteamrepository_124',['ITeamRepository',['../interface_football_record_1_1_repository_1_1_i_team_repository.html',1,'FootballRecord::Repository']]]
];
