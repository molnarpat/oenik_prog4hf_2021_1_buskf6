var class_football_record_1_1_data_1_1_models_1_1_football_d_b_context =
[
    [ "FootballDBContext", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#ae9d03f5e581537e16c21a860d327f732", null ],
    [ "FootballDBContext", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a0a85f35a9eb9801e40e489012b8de619", null ],
    [ "OnConfiguring", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#ab588ddadd008ab5e3ac38cb84306286a", null ],
    [ "OnModelCreating", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#ad200b1df3ecca9ecbe8bac153e6613ad", null ],
    [ "Coaches", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a6f07f14582650f60f74dc3611744ae51", null ],
    [ "Players", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a05f7e47169b39e46a999819e1a14268e", null ],
    [ "Teams", "class_football_record_1_1_data_1_1_models_1_1_football_d_b_context.html#a32c891070520a946315dcb1e66c7bfa9", null ]
];