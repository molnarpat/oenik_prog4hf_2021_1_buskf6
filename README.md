Footballrecord


Funkciólista:

- Labdarúgók listázása / hozzáadása / módosítása / törlése
- Edzők listázása / hozzáadása / módosítása / törlése
- Csapatok listázása / hozzáadása / módosítása / törlése
- Edzővel rendelkező játékosok listázása
- A legalacsonyabb költségvetésű csapat játékosainak listázása
- Játékosok átlagos teljesítménye a csapatonként
- A csapatok átlagéletkor alapján növekvő sorrendbe rendezése
